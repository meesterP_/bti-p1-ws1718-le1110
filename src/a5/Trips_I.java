package a5;


import cards.Card;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Das Interface Trips_I
 * <ul>
 *     <li>beschreibt einen Drilling und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und fordert die entsprechenden Methoden ein.</li>
 * </ul>
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public interface Trips_I {
    
    /**
     * Diese Methode soll die jeweilige Karte des Drillings abliefern.
     * Die jeweilge Karte wird ueber ihre Position in der Eintreffreihenfolge identifiziert.
     * Die zuerst eingetroffene Karte soll beim aktuellen Parameter-Wert 0,
     * die als zweites eingetroffene Karte beim aktuellen Parameter-Wert 1 und
     * die zuletzt eingetroffene Karte beim aktuellen Parameter-Wert 2 abgeliefert werden.
     * Im Falle eines ungueltigen Wertes des aktuellen Parameters ( also eines Wertes ausserhalb von [0,..,2] )
     * soll eine sinnvolle Reaktion erfolgen.
     *
     * @param no bestimmt die jeweilige Karte mit 0 fuer die zuerst eingetroffene Karte und 2 fuer die zuletzt eingetroffene Karte.
     * @return die jeweilige Karte des Drillings.
     */
    Card get( final int no );
    
}//interface
