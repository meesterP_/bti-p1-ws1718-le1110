package a5;


import cards.*;
import cards.Card.*;
import static cards.Card.*;
import static cards.Card.Constant.*;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Das Interface CardProcessor_I
 * <ul>
 *     <li>beschreibt einen CardProcessor und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und fordert die entsprechenden Methoden ein.</li>
 * </ul>
 * Die von Ihnen zu implementierende Klasse CardProcessor reagiert auf eine jeweils (neu) erscheinende Karte.
 * Diese Karte wird als Parameter an die Methode process uebergeben.
 * Die Methode process hat die Aufage, die Reaktion auf die jeweilige Karte umzusetzen.<br />
 * Der CardProcessor muss
 * <ul>
 *     <li>einen oeffentlichen Konstruktor aufweisen, der der folgenden Signatur genuegt:<br />
 *         <code>CardProcessor()</code>
 *     </li>
 * </ul>
 * Bemerkung:<br />
 * Es wird das Ihnen aus der Programmierenveranstaltung bekannte card-Package verwendet.  Es liegt ein Poker-Blatt vor.
 * Ein Spielkartenblatt enthaelt also 52 (verschiedene)Karten = 4 Farben * 13 Raenge.<br />
 * <br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse findet sich auf dem Aufgabenzettel.<br />
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public interface CardProcessor_I {
    
    /**
     * Diese Methode verarbeitet eine (Spiel-)Karte.<br />
     * Der von Ihnen zu implementierende CardProcessor reagiert auf die jeweils (neu) erscheinende Karte.
     * Die Reaktion wird von der Methode process realisiert.
     * Die jeweils neu erscheinende Karte wird konkret als Parameter an die Methode process uebergeben
     * und muss geeignet intern verabeitet werden.<br />
     * Sobald ein Drillling (3 Karten vom gleichen Rang) vorliegt,
     * soll dieser Drillling (also die entsprechenden 3 Karten)
     * die Eintreff-Reihenfolge erhaltend als Rueckgaberwert der Methode zurueckgegeben werden.
     * Karten, die bereits Teil eines Drillings waren, duerfen nicht fuer die Bildung eines anderen Drillings verwendet werden.
     * <br />
     * @param card bestimmt die (neue) Karte, die zu verarbeiten ist.
     * @return  der jeweilige Drilling unmittelbar nachdem er gebildet werden kann<br />
     *          und sonst null.
     */
    Trips_I process( final Card card );
    
    
    /**
     * Diese Methode setzt einen moeglichen (internen) Zustand auf den Ausgangswert bzw. die Starteinstellung zurueck.<br />
     * (reset macht halt einen reset ;-) Der Name sollte selbsterklaerend sein.)
     */
    void reset();
    
}//interface
