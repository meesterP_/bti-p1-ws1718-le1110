package a5;

import cards.*;
import cards.Card.*;
import static cards.Card.*;
import static cards.Card.Constant.*;
import java.util.HashMap;
import java.util.Map;

public class CardProcessor implements CardProcessor_I {
    private Map<Card.Rank, Integer> mapCounter;
    private Map<Card.Rank, Trips> mapTrips;
    
    //Constructor
    public CardProcessor(){
        mapCounter = new HashMap<Card.Rank, Integer>();
        mapTrips = new HashMap<Card.Rank, Trips>();
    }

    @Override
    public Trips_I process(Card card) {
        Rank rank = card.getRank();
        // if card is found
        if (mapCounter.containsKey(rank)) {
            // add one to counter
            int newValue = mapCounter.get(rank) + 1; 
            mapCounter.put(rank, newValue);
            mapTrips.get(rank).addCard(card);
            if (mapCounter.get(rank) >= 3 ) {
                mapCounter.remove(rank);
                Trips tripsToReturn = mapTrips.get(rank);
                mapTrips.remove(rank);
                return tripsToReturn;
            } else {
                return null;
            }
        } else {
            mapCounter.put(rank, 1);
            mapTrips.put(rank, new Trips(card));
            return null;
        }
    }

    @Override
    public void reset() {
        mapCounter.clear();
        mapTrips.clear();
    }

}
