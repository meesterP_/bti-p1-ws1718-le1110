package a5;


import static cards.Card.*;
import static cards.Card.Constant.*;
import static org.junit.Assert.*;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.Modifier;
//
import java.util.Arrays;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import cards.*;
//
import supportC1x00.hri.HRI;
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben
 * und Ihnen nur eine Idee geben, wie gut Sie die Aufgabe geloest haben.<br />
 * Es ist insbesondere NICHT geplant, dass Sie alle Tests verstehen. Vermutlich gibt es so viele Tests,
 * dass Sie gar nicht die Zeit haben sich in alle Tests einzuarbeiten.<br />
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann NICHT als Beweis dienen,
 * dass der Code fehlerfrei ist. Es liegt in Ihrer Verantwortung sicher zu stellen,
 * dass Sie fehlerfreien Code geschrieben haben.<br />
 * Bei der Bewertung werden u.U. andere - konkret : modifizierte und haertere Tests - verwendet.
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet  ?=>? 0 Punkte?). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "CardProcessor";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final CardProcessor_I cardProcessor = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
                TS.printDetailedInfoAboutObject( cardProcessor, "cardProcessor" );
                //
                if( TS.isActualMethod( cardProcessor.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  cardProcessor.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  cardProcessor.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /*-* Existenz-Test: "Trips". */
    /* Es ist keine Klasse mit dem Namen Trips eingefordert - es gibt nur das Interface Trips_I
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_Trips(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Trips";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENZ ),  new TestResult( testName, countsOnePoint ) );
        }//If
    }//method()
    */
    
    /** Existenz-Test: "CardProcessor". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_CardProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
         }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "CardProcessor". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_CardProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                     TS.isClass(             classUnderTest ));                                                                                      // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(      classUnderTest, "CardProcessor_I" ));
            assertTrue( "requested constructor missing",    TS.isConstructor(       classUnderTest, null ));
            assertTrue( "false constructor access modifier",TS.isAccessModifierSet( classUnderTest, null, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()   
    
    /** Test Eigenschaften "CardProcessor" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_CardProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "process", Trips_I.class, new Class<?>[]{ Card.class } ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "reset",   void.class,    null ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "process", Trips_I.class, new Class<?>[]{ Card.class }, Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "reset",   void.class,    null,                         Modifier.PUBLIC )); // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CardProcessor" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_CardProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CardProcessor" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_CardProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "CardProcessor" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_CardProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: CardProzessor erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_CardProcessor(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final CardProcessor_I cardProzessor = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "reset()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_reset(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            cp.reset();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "process()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_process(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            Trips_I justTakeIt = cp.process( CJ );
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Einfacher Test: Funktion "process()" fuer 1 Karte. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_processSingleCard(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertNull( cp.process( DJ ));
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "process()" fuer 3 Karten. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_process3CardsWithoutTrips(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( final Card card : new Card[]{ D2, H3, S4 }){
                assertNull( "no trips expected", cp.process( card ));
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()    
    
    /** Einfacher Test: Funktion "process()" fuer 13 Karten. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_process13CardsWithoutTrips(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( final Card card : new Card[]{ D2, H3, S4, C5, D6, H7, S8, S9, CT, DJ, HQ, SK, DA }){
                assertNull( "no trips expected", cp.process( card ));
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "process()" fuer 3 Karten. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_process3CardsWithTrips(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( final Card card : new Card[]{ HQ, CQ }){
                assertNull( "no trips expected", cp.process( card ));
            }//for
            final Trips_I rc = cp.process( SQ );
            assertNotNull( rc );
            assertTrue(
                String.format( "false result -> %s%s%S is NOT HQ CQ SQ",  rc.get(0), rc.get(1), rc.get(2) ),
                Arrays.equals( new Card[]{HQ,CQ,SQ}, new Card[]{ rc.get(0), rc.get(1), rc.get(2) } )
            );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    /** Test: Funktion "reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_reset_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( final Card card : new Card[]{ CA, SA }){
                assertNull( cp.process( card ));
            }//for
            cp.reset();
            //
            assertNull( cp.process( HA ));
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    /** Test: Funktion "reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_reset_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( final Card card : new Card[]{ CA, CK, CQ, CJ, CT, C9, C8, C7, C6, C5, C4, C3, C2, SA, SK, SQ, SJ, ST, S9, S8, S7, S6, S5, S4, S3, S2 }){
                assertNull( cp.process( card ));
            }//for
            cp.reset();
            //
            assertNull( cp.process( HA ));
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    /** Test: Funktion "reset()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_reset_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=13; --stillToDo>=0; ){
                for( final Card card : new Card[]{ CA, CK, CQ, CJ, CT, C9, C8, C7, C6, C5, C4, C3, C2, SA, SK, SQ, SJ, ST, S9, S8, S7, S6, S5, S4, S3, S2 }){
                    assertNull( cp.process( card ));
                }//for
                cp.reset();
                //
                for( final Card card : new Card[]{ HA, HK, HQ, HJ, HT, H9, H8, H7, H6, H5, H4, H3, H2, DA, DK, DQ, DJ, DT, D9, D8, D7, D6, D5, D4, D3, D2 }){
                    assertNull( cp.process( card ));
                }//for
                cp.reset();
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    // Hier wird "Trips"-Kontrolle nachgeholt
    // Es wurde nur das Trips_I-Interface eingefordert und keine Klasse - daher ist Klassenname unbekannt ;-)
    // Test ist C-Level, da process() funktionieren muss - einzige Weg um an Trips_I-Objekt zu kommen und damit an die implementierende Klasse
    /** Konrolle der Trips_I-Implementierung */
    @Test( timeout = commonLimit )
    public void tol_3n_check_Trips_I(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( final Card card : new Card[]{ DT, CT }){
                cp.process( card ); // Fehler die "hier" passieren sind fuer diesen test nicht relevant - "HIER" soll die das Objekt und damit das Interface implementierende Klasse untersucht werden
            }//for
            final Trips_I trips = cp.process( ST );
            assertNotNull( trips );
            final Class<?> classUnderTest = trips.getClass();
            //\=> nun kann es losgehen -> ab "hier" Level: 1e/2b
            
            // Kontrolle: Eigenschaften der "unbekannten" Klasse.
            assertTrue(                                     TS.isClass(        classUnderTest ));                       // always true ;-)
            // Keine Anforderungen an den AccessModifier der Klasse
            assertTrue( "requested supertype missing",      TS.isImplementing( classUnderTest, "Trips_I" ));            // always true ;-)
            // Keine Anforderungen an den Konstruktor
            
            // Kontrolle: Access Modifier Methoden.
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "get", Card.class, new Class<?>[]{ int.class } ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "get", Card.class, new Class<?>[]{ int.class }, Modifier.PUBLIC ));
            
            // Kontrolle: Access Modifier Variablen.
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
            
            // Kontrolle: Schreibweise Methoden.
            final String faultExampleM = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExampleM ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExampleM ));
            }//if
            
            // Kontrolle: Schreibweise Variablen.
            final String faultExampleV = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExampleV ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExampleV ));
            }//if
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfacher Test: Funktion "process()" ???. */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_processSequenceWithQuads(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final CardProcessor_I cp = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( final Card card : new Card[]{ HQ, CQ }){
                assertNull( "no trips expected", cp.process( card ));
            }//for
            final Trips_I rc = cp.process( SQ );
            assertNotNull( rc );
            assertTrue(
                String.format( "false result -> %s%s%S is NOT HQ CQ SQ",  rc.get(0), rc.get(1), rc.get(2) ),
                Arrays.equals( new Card[]{HQ,CQ,SQ}, new Card[]{ rc.get(0), rc.get(1), rc.get(2) } )
            );
            assertNull( "no trips expected", cp.process( DQ ));
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###    
    
    /** Stress-Test: Funktion "process()" - test is random based as result of Deck-constructor usage. */
    @Test( timeout = commonLimit )
    public void tol_4s_additionalTest_checkAgainstReferenceProcessBehavior(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
        try{
            
            for( int run=1; run <=1000; run ++ ){
                final CardProcessor_I cpExa = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));   // Card Processor EXAminee
                final CardProcessor_I cpRef = new HRI.RCP();                                                                // Card Processor REFerence
                final Deck deck = new Deck();
                for( int cardCnt = 0; cardCnt<52; cardCnt++ ){
                    final Card card = deck.deal();
                    final Trips_I rcExa = cpExa.process( card );
                    final Trips_I rcRef = cpRef.process( card );
                    if( null==rcRef ){
                        assertNull( rcExa );
                    }else{
                        assertNotNull( rcExa );
                        //HERE>>
                        assertTrue(
                            String.format( "false result @(r#%d,c#%d) -> %s is NOT %s",  run, cardCnt, rcExa, rcRef ),
                            Arrays.equals( new Card[]{ rcExa.get(0), rcExa.get(1), rcExa.get(2) }, new Card[]{ rcRef.get(0), rcRef.get(1), rcRef.get(2) } )
                        );
                        //<<HERE
                    }//if
                }//for
            }//for
            
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }finally{
            System.out.flush();
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    /** Stress-Test: Funktion "process()" - test is random based as result of Deck-constructor usage. */
    @Test( timeout = commonLimit )
    public void tol_4s_additionalTest_checkAgainstReferenceResetBehavior(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CardProcessor";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
        try{
            
            for( int run=1; run <=1000; run ++ ){
                final CardProcessor_I cpExa = (CardProcessor_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));   // Card Processor EXAminee
                final CardProcessor_I cpRef = new HRI.RCP();                                                                // Card Processor REFerence
                final Deck deck = new Deck();
                Trips_I rcRef;
                do{
                    final Card card = deck.deal();
                    rcRef = cpRef.process( card );
                    if( null!=rcRef ){
                        cpExa.reset();
                        final Trips_I rcExa = cpExa.process( card );
                        assertNull( rcExa );
                    }//if
                }while( null!=rcRef );
                cpRef.reset();
                cpExa.reset();
            }//for
            
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }finally{
            System.out.flush();
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 21;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A5;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
  //final static private boolean enableDbgOutputAfterEachTest  =  ( 0 != ( dbgConfigurationVector & 0x0080_0000 ));
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
