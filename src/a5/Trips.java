package a5;

import cards.Card;

public class Trips implements Trips_I{
    private Card card;
    private Card[] cards;
    private int counter = 0;
    
    public Trips(Card card) {
        this.card = card;
        cards = new Card[3];
        addCard(card);
    }
    
    public void addCard(Card card) {
        cards[counter] = card;
        counter++;
    }

    @Override
    public Card get(int no) {
        if (0 > no || 2 < no) return null;
        return cards[no];
    }

}
