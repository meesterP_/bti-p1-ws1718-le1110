package a3;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben
 * und Ihnen nur eine Idee geben, wie gut Sie die Aufgabe geloest haben.<br />
 * Es ist insbesondere NICHT geplant, dass Sie alle Tests verstehen. Vermutlich gibt es so viele Tests,
 * dass Sie gar nicht die Zeit haben sich in alle Tests einzuarbeiten.<br />
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann NICHT als Beweis dienen,
 * dass der Code fehlerfrei ist. Es liegt in Ihrer Verantwortung sicher zu stellen,
 * dass Sie fehlerfreien Code geschrieben haben.<br />
 * Bei der Bewertung werden u.U. andere - konkret : modifizierte und haertere Tests - verwendet.
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v0
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet  ?=>? 0 Punkte?). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "WordAnalyzer";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            final String testWord = "anything";
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final WordAnalyzer_I wordAnalyzer = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testWord } ));
                TS.printDetailedInfoAboutObject( wordAnalyzer, "wordAnalyzer" );
                //
                if( TS.isActualMethod( wordAnalyzer.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  wordAnalyzer.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  wordAnalyzer.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "WordAnalyzer". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_WordAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "WordAnalyzer". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_WordAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            //
            assertTrue(                                     TS.isClass(             classUnderTest ));                                                                      // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(      classUnderTest, "WordAnalyzer_I" ));
            assertTrue( "requested constructor missing",    TS.isConstructor(       classUnderTest, new Class<?>[]{ String.class } ));
            assertTrue( "false constructor access modifier",TS.isAccessModifierSet( classUnderTest, new Class<?>[]{ String.class }, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
     
    /** Test Eigenschaften "WordAnalyzer" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_WordAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "isPalindrom", boolean.class, null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "countVocals", int.class,     null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "getWord",     String.class,  null ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "isPalindrom", boolean.class, null, Modifier.PUBLIC ));     // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "countVocals", int.class,     null, Modifier.PUBLIC ));     // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "getWord",     String.class,  null, Modifier.PUBLIC ));     // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "WordAnalyzer" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_WordAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "WordAnalyzer" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_WordAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "WordAnalyzer" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Grundsaetzlicher Test: WordAnalyzer erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_WordAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testActualParameter = "anything";
        try{
            @SuppressWarnings("unused")
            final WordAnalyzer_I wordAnalyzer = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testActualParameter } ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "getWord()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_getWord(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testActualParameter = "anything";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testActualParameter } ));
            String justTakeIt = wa.getWord();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%s\" -> %s",  testActualParameter, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_countVocals(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testActualParameter = "anything";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testActualParameter } ));
            int justTakeIt = wa.countVocals();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%s\" -> %s",  testActualParameter, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_isPalindrom(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final String testActualParameter = "anything";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testActualParameter } ));
            boolean justTakeIt = wa.isPalindrom();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%s\" -> %s",  testActualParameter, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   getWord()
    //###
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Funktions-Test: "getWord()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getWord_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String testWord = "anything";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testWord } ));
            assertEquals( testWord, wa.getWord() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
        
    /** Funktions-Test: "getWord()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getWord_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String testWord = "";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testWord } ));
            assertEquals( testWord, wa.getWord() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    /** Funktions-Test: "getWord()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getWord_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String testWord = "X%€äÖüßÄöÜ-~*+@$^X";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testWord } ));
            assertEquals( testWord, wa.getWord() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###    
    
    /** Funktions-Test: "getWord()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_getWord_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String testWord = null;
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testWord } ));
            String justTakeIt = wa.getWord();
            fail( "undetected illegal argument   ->   null" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName,countsOnePoint ) );
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   countVocals()
    //###
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Sehr einfacher Test: "countVocals". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_countVocals_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String actualTestParameter = "bcdfghjklmnpqrstvwxyz";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { actualTestParameter } ));
            assertTrue( 0 == wa.countVocals() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Sehr einfacher Test: "countVocals". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_countVocals_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String actualTestParameter = "aeiouAEIOU";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { actualTestParameter } ));
            assertTrue( 1 <= wa.countVocals() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no1(){
        subTestBehavior_countVocals( "Test",  1,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no2(){
        subTestBehavior_countVocals( "ede",  2,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no3(){
        subTestBehavior_countVocals( "OtTo",  2,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no4(){
        subTestBehavior_countVocals( "reliefpfeiler",  6,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no5(){
        subTestBehavior_countVocals( "vwxwv",  0,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no6(){
        subTestBehavior_countVocals( "mhmm",  0,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no7(){
        subTestBehavior_countVocals( "aOuEiuIeUoA",  11,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no8(){
        subTestBehavior_countVocals( "hochenwiseler",  5,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_countVocals_no9(){
        subTestBehavior_countVocals( "polygenelubricants",  6,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###    
    
    /** Funktions-Test: "countVocals()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_countVocals_no1(){
        subTestBehavior_countVocals( "",  0,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "countVocals()" -> null. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_countVocals_no99(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String testWord = null;
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testWord } ));
            int justTakeIt = wa.countVocals();
            fail( "undetected illegal argument   ->   null" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private void subTestBehavior_countVocals(
        final String testParameter,                                             // test paramter - word that shall be checked for the number of vocals it contains
        final int expectedResult,                                               // expected resul of countVocals()
        final int points,                                                       // points in case of success
        final TL testLevel,                                                     // test level
        final String testName                                                   // name of test method that is responsible fot the test
    ){
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testParameter } ));
            assertEquals( expectedResult, wa.countVocals() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( testLevel, exercise, TC.BEHAVIOR ),  new TestResult( testName, points ) );
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   isPalindrom()
    //###
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Einfacher Test: Funktion "isPalindrom()" - negativ */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isPalindromFalse(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String actualTestParameter = "WXyz";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { actualTestParameter } ));
            assertFalse( wa.isPalindrom() );
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%s\" -> %s",  actualTestParameter, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()" - positiv */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isPalindromTrue(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String actualTestParameter = "xxx";
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { actualTestParameter } ));
            assertTrue( wa.isPalindrom() );
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%s\" -> %s",  actualTestParameter, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith1Letter(){
        subTestBehavior_isPalindrom( "H",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith2lettersFalse_no1(){
        subTestBehavior_isPalindrom( "qp",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith2lettersFalse_no2(){
        subTestBehavior_isPalindrom( "db",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith2lettersFalse_no3(){
        subTestBehavior_isPalindrom( "EK",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith2lettersTrue(){
        subTestBehavior_isPalindrom( "xx",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
        
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith3lettersMixedCase(){
        subTestBehavior_isPalindrom( "Ede",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith4lettersLowerCase(){
        subTestBehavior_isPalindrom( "xxxx",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith4lettersUpperCase(){
        subTestBehavior_isPalindrom( "WVVW",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWith4lettersMixedCase(){
        subTestBehavior_isPalindrom( "OtTo",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSomeWord_no1(){
        subTestBehavior_isPalindrom( "xfjfxtf",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSomeWord_no2(){
        subTestBehavior_isPalindrom( "polygenelubricants",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no1(){
        subTestBehavior_isPalindrom( "detartrated",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
   /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no2(){
        subTestBehavior_isPalindrom( "tattarrattat",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no3(){
        subTestBehavior_isPalindrom( "Regalager",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no4(){
        subTestBehavior_isPalindrom( "reLieFPfEileR",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no5(){
        subTestBehavior_isPalindrom( "vwxwv",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
   /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no6(){
        subTestBehavior_isPalindrom( "aOuEiuIeUoA",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no7(){
        subTestBehavior_isPalindrom( "xxxxxxxxxxxxxxixxxxxxxxxxxxxxxx",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no8(){
        subTestBehavior_isPalindrom( "xxxxxxxxxxxxxxxxixxxxxxxxxxxxxx",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no9(){
        subTestBehavior_isPalindrom( "xxxxxxxxxxxxxxxqpxxxxxxxxxxxxxxx",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no10(){
        subTestBehavior_isPalindrom( "hochenwiseler",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no11(){
        subTestBehavior_isPalindrom( "mhmm",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no12(){
        subTestBehavior_isPalindrom( "XHH",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no13(){
        subTestBehavior_isPalindrom( "HHX",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no14(){
        subTestBehavior_isPalindrom( "XHHHH",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no15(){
        subTestBehavior_isPalindrom( "HXHHH",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no16(){
        subTestBehavior_isPalindrom( "HHHXH",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no17(){
        subTestBehavior_isPalindrom( "HHHHX",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no18(){
        subTestBehavior_isPalindrom( "xHHHHH",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no19(){
        subTestBehavior_isPalindrom( "HHHHHx",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no20(){
        subTestBehavior_isPalindrom( "HnHnHn",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no21(){
        subTestBehavior_isPalindrom( "X",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no22(){
        subTestBehavior_isPalindrom( "Xxx",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no23(){
        subTestBehavior_isPalindrom( "xxX",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no24(){
        subTestBehavior_isPalindrom( "Xxxx",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no25(){
        subTestBehavior_isPalindrom( "xXxx",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no26(){
        subTestBehavior_isPalindrom( "xxXx",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no27(){
        subTestBehavior_isPalindrom( "xxxX",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no28(){
        subTestBehavior_isPalindrom( "XHX",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no29(){
        subTestBehavior_isPalindrom( "XHXHX",  true,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isPalindromWithSeveralDifferentWords_no30(){
        subTestBehavior_isPalindrom( "HnHnHn",  false,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###    
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no1(){
        subTestBehavior_isPalindrom( "",  true,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no2(){
        subTestBehavior_isPalindrom( ":",  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no3(){
        subTestBehavior_isPalindrom( "--",  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no4(){
        subTestBehavior_isPalindrom( "...",  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no5(){
        subTestBehavior_isPalindrom( "Ede,Ede,Ede",  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no6(){
        subTestBehavior_isPalindrom( "Otto neben Otto",  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no7(){
        subTestBehavior_isPalindrom( "\"Anna neben Anna\"",  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no8(){
        subTestBehavior_isPalindrom( "" + 'x',  true,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no9(){
        subTestBehavior_isPalindrom( "" + (char)( 0xb5 ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no10(){
        subTestBehavior_isPalindrom( "" + (char)( 0xdf ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no11(){
        subTestBehavior_isPalindrom( "" + (char)( 0xe4 ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no12(){
        subTestBehavior_isPalindrom( "" + (char)( 0xf6 ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no13(){
        subTestBehavior_isPalindrom( "" + (char)( 0xfc ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no14(){
        subTestBehavior_isPalindrom( "" + (char)( 0xef ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no15(){
        subTestBehavior_isPalindrom( "" + (char)( 0xeb ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no16(){
        subTestBehavior_isPalindrom( "" + (char)( 0xe8 ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no17(){
        subTestBehavior_isPalindrom( "" + (char)( 0xe9 ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" verschiedene Testwoerter. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no18(){
        subTestBehavior_isPalindrom( "" + (char)( 0xea ),  false,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "isPalindrom()" -> null. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_isPalindrom_no99(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        
        final String testWord = null;
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testWord } ));
            boolean justTakeIt = wa.isPalindrom();
            fail( "undetected illegal argument   ->   null" );
        }catch( final TestSupportException ex ){
            TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( testName, ex );
        }catch( final NullPointerException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ) );
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    private void subTestBehavior_isPalindrom(
        final String testParameter,                                             // test paramter - word that shall be checked if it is a palindrom
        final boolean expectedResult,                                           // expected result of palindrom-test
        final int points,                                                       // points in case of success
        final TL testLevel,                                                     // test level
        final String testName                                                   // name of test method that is responsible fot the test
    ){
        final String requestedRefTypeName = "WordAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            /*scope*/{
                final String guardTestDataPositiv = "xxx";
                final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { guardTestDataPositiv } ));
                assertTrue( "Guard test failed - palindrom was NOT detected",  wa.isPalindrom() );
            }//scope
            /*scope*/{
                final String guardTestDataNegativ = "WXyz";
                final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { guardTestDataNegativ } ));
                assertFalse( "Guard test failed - FALSE palindrom detected",  wa.isPalindrom() );
            }//scope
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash/error yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final WordAnalyzer_I wa = (WordAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ String.class },  new Object[] { testParameter } ));
            assertEquals( expectedResult, wa.isPalindrom() );
        }catch( final TestSupportException ex ){
            fail( String.format( "\"%s\" -> %s",  testParameter, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( testLevel, exercise, TC.BEHAVIOR ),  new TestResult( testName, points ) );
        }//if
    }//method()
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()                                                 // BlueJ keeps state, otherwise
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 90;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A3;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
  //final static private boolean enableDbgOutputAfterEachTest  =  ( 0 != ( dbgConfigurationVector & 0x0080_0000 ));
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
