package a3;

public class WordAnalyzer implements WordAnalyzer_I {
    // Attributes
    private String word;

    // Constructor
    public WordAnalyzer(String word) {
        assert word  != null : "illegal parameter!";
        this.word = word;
    }

    // Methods
    @Override
    public boolean isPalindrom() {
        if (word == "") return true;
        char[] wordAsArray = word.toLowerCase().toCharArray();
        int firstPos = 0;
        int lastPos = wordAsArray.length - 1;
        char first = wordAsArray[firstPos];  
        char last = wordAsArray[lastPos];

        while (first == last) {
            if (first < 'a' || 'z' < first) return false;
            if (last < 'a' || 'z' < last) return false;
            if (firstPos >= lastPos) {
                return true;
            }
            firstPos++;
            lastPos--;
            first = wordAsArray[firstPos];
            last = wordAsArray[lastPos];
        }
        return false;
    }

    @Override
    public int countVocals() {
        String vocals = "aeiou";
        int amountOfVocals = 0;
        for (int i = 0; i < word.length(); i++) {
            String currentChar = Character.toString(word.charAt(i));
            if (vocals.contains(currentChar.toLowerCase())) {
                amountOfVocals++;
            }
        }
        return amountOfVocals;
    }

    @Override
    public String getWord() {
        return word;
    }

}
