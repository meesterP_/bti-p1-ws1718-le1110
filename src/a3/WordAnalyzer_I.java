package a3;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Das Interface WordAnalyzer_I
 * <ul>
 *     <li>beschreibt einen WordAnalyzer und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und fordert die entsprechenden Methoden ein.</li>
 * </ul>
 * Die von Ihnen zu implementierende Klasse WordAnalyzer muss
 * <ul>
 *     <li>einen oeffentlichen Konstruktor aufweisen, der der folgenden Signatur genuegt:<br />
 *         <code>WordAnalyzer( String )</code>
 *     </li>
 * </ul>
 * Der als Parameter uebergebene Text wird von den nachfolgend eingeforderten Methoden benoetigt,
 * da diese Methoden unterschiedliche Auswertungen des Textes implementieren.<br />
 * Ein Text wird als Wort gewertet, wenn der Text nur aus Buchstaben des modernen lateinischen Alphabets besteht.<br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse findet sich auf dem Aufgabenzettel.
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v0
 */
public interface WordAnalyzer_I {
    
    /**
     * Diese Methode ueberprueft den (zuvor dem Konstruktor uebergebene) Text darauf, ob der Text ein (Wort-)Palindrom ist.
     * Der Text darf nur aus Buchstaben des modernen lateinischen Alphabets bestehen um als Palindrom anerkannt zu werden.
     * Leere Woerter sind als Palindrom zu werten.
     *
     * @return <code>true</code>, wenn ein Palindrom vorliegt und
     *         sonst <code>false</code>.
     */
    boolean isPalindrom();
    
    /**
     * Diese Methode zaehlt die Vokale im (zuvor dem Konstruktor uebergebenen) Wort.
     *
     * @return Anzahl der Vokale.
     */
    int countVocals();
    
    /**
     * Diese Methode liefert das Wort, das untersucht wird.
     */
    String getWord();
    
}//interface
