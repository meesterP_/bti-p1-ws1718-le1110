package a4;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.Modifier;
//
import java.util.Map;
import java.util.TreeMap;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;
import supportC1x00.YattbCounter;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben
 * und Ihnen nur eine Idee geben, wie gut Sie die Aufgabe geloest haben.<br />
 * Es ist insbesondere NICHT geplant, dass Sie alle Tests verstehen. Vermutlich gibt es so viele Tests,
 * dass Sie gar nicht die Zeit haben sich in alle Tests einzuarbeiten.<br />
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann NICHT als Beweis dienen,
 * dass der Code fehlerfrei ist. Es liegt in Ihrer Verantwortung sicher zu stellen,
 * dass Sie fehlerfreien Code geschrieben haben.<br />
 * Bei der Bewertung werden u.U. andere - konkret : modifizierte und haertere Tests - verwendet.
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet  ?=>? 0 Punkte?). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview_ArrayAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "ArrayAnalyzer";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            final long[][][] testArray = new long[][][]{{{0L}}};
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final ArrayAnalyzer_I arrayAnalyzer = (ArrayAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][][].class },  new Object[] { testArray } ));
                TS.printDetailedInfoAboutObject( arrayAnalyzer, "arrayAnalyzer" );
                //
                if( TS.isActualMethod( arrayAnalyzer.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  arrayAnalyzer.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  arrayAnalyzer.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /*-* Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet  ?=>? 0 Punkte?). */
    /* Es ist keine Klasse mit dem Namen CensusElem eingefordert - es gibt nur das Interface CensusElem_I
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview_CensusElem(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0100 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "CensusElem";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            try{
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsZeroPoints ) );
        }//if
    }//method()
    */
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "ArrayAnalyzer". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_ArrayAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /*-*
     * Existenz-Test: "CensusElem".
     */
    /* Es ist keine Klasse mit dem Namen CensusElem eingefordert - es gibt nur das Interface CensusElem_I
    @Test( timeout = commonLimit )
    public void testBasicClassExistence_CensusElem(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CensusElem";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENZ ),  new TestResult( testName, 1 ) );
        }//if
    }//method()
    */
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "ArrayAnalyzer". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_ArrayAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
             final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                     TS.isClass(             classUnderTest ));                                                                                      // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(      classUnderTest, "ArrayAnalyzer_I" ));
            assertTrue( "requested constructor missing",    TS.isConstructor(       classUnderTest, new Class<?>[]{ long[][][].class } ));
            assertTrue( "false constructor access modifier",TS.isAccessModifierSet( classUnderTest, new Class<?>[]{ long[][][].class }, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, 1 ) );
        }//if
    }//method()
     
    /** Test Eigenschaften "ArrayAnalyzer" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_ArrayAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "computeXorOfAllElements", long.class,           null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "getCensus",               CensusElem_I[].class, null ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "computeXorOfAllElements", long.class,           null, Modifier.PUBLIC ));  // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "getCensus",               CensusElem_I[].class, null, Modifier.PUBLIC ));  // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ArrayAnalyzer" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_ArrayAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ArrayAnalyzer" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_ArrayAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ArrayAnalyzer" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_ArrayAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /*-* ... */
    /* Es ist keine Klasse mit dem Namen CensusElem eingefordert - es gibt nur das Interface CensusElem_I
    @Test( timeout = commonLimit )
    public void testBasicAttributes_CensusElem(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "CensusElem";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            //
            assertTrue(                                     TS.isClass( classUnderTest ));                                                                                      // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing( classUnderTest, "CensusElem_I" ));
            //
            assertTrue( "requested method missing",         TS.isMethod( classUnderTest, "getCount", int.class,  null ));
            assertTrue( "requested method missing",         TS.isMethod( classUnderTest, "getElem",  long.class, null ));
            //
            assertTrue( "false method access modifier",     TS.isAccessModifierSet( classUnderTest, "getCount", int.class,  null, Modifier.PUBLIC ));  // -D interface ;-)
            assertTrue( "false method access modifier",     TS.isAccessModifierSet( classUnderTest, "getElem",  long.class, null, Modifier.PUBLIC ));  // -D interface ;-)
            //
            assertTrue( "false field access modifier",      TS.allFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.ATTRIBUTE ),  new TestResult( testName, 1 ) );
        }//if
    }//method()
    */
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: ArrayAnalyzer erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_ArrayAnalyzer(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][][] testActualParameter = new long[][][]{{{0L}}};
        try{
            @SuppressWarnings("unused")
            final ArrayAnalyzer_I ArrayAnalyzer = (ArrayAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][][].class },  new Object[] { testActualParameter } ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
     
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_computeXorOfAllElements(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][][] testActualParameter = new long[][][]{{{0L}}};
        try{
            @SuppressWarnings("unused")
            final ArrayAnalyzer_I aa = (ArrayAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][][].class },  new Object[] { testActualParameter } ));
            long justTakeIt = aa.computeXorOfAllElements();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_getCensus(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        final long[][][] testActualParameter = new long[][][]{{{0L}}};
        try{
            @SuppressWarnings("unused")
            final ArrayAnalyzer_I aa = (ArrayAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][][].class },  new Object[] { testActualParameter } ));
            CensusElem_I[] justTakeIt = aa.getCensus();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    // Hier wird "CensusElem"-Kontrolle nachgeholt
    // Es wurde nur das CensusElem_I-Interface eingefordert und keine Klasse - daher ist Klassenname unbekannt ;-)
    // Test ist C-Level, da process() funktionieren muss - einzige Weg um an CensusElem_I-Objekt zu kommen und damit an die implementierende Klasse
    /** Konrolle der CensusElem_I-Implementierung */
    @Test( timeout = commonLimit )
    public void tol_3n_check_CensusElem_I(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final long[][][] testParameter = { { { 1, 2 }, { 3, 4 } }, { { 5, 6 }, { 7, 8 } } };
            final ArrayAnalyzer_I aa = (ArrayAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][][].class },  new Object[] { testParameter } ));
            final CensusElem_I[] censusElemArray = aa.getCensus();
            assertNotNull( censusElemArray );
            assertTrue( 0 < censusElemArray.length );
            final CensusElem_I censusElem = censusElemArray[0];
            assertNotNull( censusElem );
            final Class<?> classUnderTest = censusElem.getClass();
            //\=> nun kann es losgehen -> ab "hier" Level: 1e/2b
            
            // Kontrolle: Eigenschaften der "unbekannten" Klasse.
            assertTrue(                                     TS.isClass(        classUnderTest ));                       // always true ;-)
            // Keine Anforderungen an den AccessModifier der Klasse
            assertTrue( "requested supertype missing",      TS.isImplementing( classUnderTest, "CensusElem_I" ));       // always true ;-)
            // Keine Anforderungen an den Konstruktor
            
            // Kontrolle: Access Modifier Methoden.
            assertTrue( "requested method missing",         TS.isMethod(            classUnderTest, "getCount", int.class,  null ));
            assertTrue( "requested method missing",         TS.isMethod(            classUnderTest, "getElem",  long.class, null ));
            assertTrue( "false method access modifier",     TS.isAccessModifierSet( classUnderTest, "getCount", int.class,  null, Modifier.PUBLIC ));  // -D interface ;-)
            assertTrue( "false method access modifier",     TS.isAccessModifierSet( classUnderTest, "getElem",  long.class, null, Modifier.PUBLIC ));  // -D interface ;-)
            
            // Kontrolle: Access Modifier Variablen.
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
            
            // Kontrolle: Schreibweise Methoden.
            final String faultExampleM = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExampleM ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExampleM ));
            }//if
            
            // Kontrolle: Schreibweise Variablen.
            final String faultExampleV = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExampleV ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExampleV ));
            }//if
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   computeXorOfAllElements()
    //###
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" aaa. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_aaa1(){
        final long[][][] arrayToBeAnalyzed = {{{1}}};
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x1L,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" aab. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_aab1(){
        final long[][][] arrayToBeAnalyzed = { { { 0b1100, 0b1010 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x6L,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" aaf. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_aaf1(){
        final long[][][] arrayToBeAnalyzed = { { { 0776, 0774, 0770, 0760, 0740, 0700, 0600 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0652L,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" aba. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_aba1(){
        final long[][][] arrayToBeAnalyzed = { { { 0b1100 }, { 0b1010 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x6L,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" abc. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_abc1(){
        final long[][][] arrayToBeAnalyzed = { { { 0xFE, 0xFC, 0xF8 }, { 0xF0, 0xE0, 0xC0 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x2AL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" acb */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_acb1(){
        final long[][][] arrayToBeAnalyzed = { { { 0xFE, 0xFC }, { 0xF8, 0xF0 }, { 0xE0, 0xC0 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x2AL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" afa. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_afa1(){
        final long[][][] arrayToBeAnalyzed = { { { 0776 }, { 0774 }, { 0770 }, { 0760 }, { 0740 }, { 0700 }, { 0600 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0652L,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" baa. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_baa1(){
        final long[][][] arrayToBeAnalyzed = { { { 0b1100 } }, { { 0b1010 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x6L,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bac. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bac1(){
        final long[][][] arrayToBeAnalyzed = { { { 0xFE, 0xFC, 0xF8 } }, { { 0xF0, 0xE0, 0xC0 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x2AL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bba. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bba1(){
        final long[][][] arrayToBeAnalyzed = { { { 0b0001 }, { 0b0010 } }, { { 0b0100 }, { 0b1000 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xFL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bbb. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bbb1(){
        final long[][][] arrayToBeAnalyzed = { { { 0xE }, { 0xB } }, { { 0x7 }, { 0xD } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xFL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bbb. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bbb2(){
        final long[][][] arrayToBeAnalyzed = { { { 0b0000_0001, 0b0000_0010 }, { 0b0000_0100, 0b0000_1000 } }, { { 0b0001_0000, 0b0010_0000 }, { 0b0100_0000, 0b1000_0000 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xFFL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bbb. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bbb3(){
        final long[][][] arrayToBeAnalyzed = { { { 1, 2 }, { 4, 5 } }, { { 6, 7 }, { 8, 9 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x2L,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bbc. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bbc1(){
        final long[][][] arrayToBeAnalyzed = { { { 0x001L, 0x002L, 0x004L }, { 0x008L, 0x010L, 0x020L } }, { { 0x040L, 0x080L, 0x100L }, { 0x200L, 0x400L, 0x800L } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xFFFL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bbd. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bbd1(){
        final long[][][] arrayToBeAnalyzed = { { { 0x0001L, 0x0002L, 0x0004L, 0x0008L }, { 0x0010L, 0x0020L, 0x0040L, 0x0080L } }, { { 0x0100L, 0x0200L, 0x0400L, 0x0800L }, { 0x1000L, 0x2000L, 0x4000L, 0x8000L } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xFFFFL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bbd. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bbd2(){
        final long[][][] arrayToBeAnalyzed = { { { 0xFFFEL, 0xFFFDL, 0xFFFBL, 0xFFF7L }, { 0xFFEFL, 0xFFDFL, 0xFFBFL, 0xFF7FL } }, { { 0xFEFFL, 0xFDFFL, 0xFBFFL, 0xF7FFL }, { 0xEFFF, 0xDFFFL, 0xBFFFL, 0x7FFFL } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xFFFFL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" bca. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_bca1(){
        final long[][][] arrayToBeAnalyzed = { { { 0xFE }, { 0xFC }, { 0xF8 } }, { { 0xF0 }, { 0xE0 }, { 0xC0 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x2AL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" cab. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_cab1(){
        final long[][][] arrayToBeAnalyzed = { { { 0xFE, 0xFC } }, { { 0xF8, 0xF0 } }, { { 0xE0, 0xC0 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x2AL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" cba. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_cba1(){
        final long[][][] arrayToBeAnalyzed = { { { 0xFE }, { 0xFC } }, { { 0xF8 }, { 0xF0 } }, { { 0xE0 }, { 0xC0 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x2AL,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "computeXorOfAllElements()" faa. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_computeXorOfAllElements_faa1(){
        final long[][][] arrayToBeAnalyzed = { { { 0776 } }, { { 0774 } }, { { 0770 } }, { { 0760 } }, { { 0740 } }, { { 0700 } }, { { 0600 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0652L,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
        
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_computeXorOfAllElements_no1(){
        final long[][][] arrayToBeAnalyzed = { { { 1 }, { 2, 3 } }, { { 4, 5, 6 }, { 7, 8, 9, 10 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xBL,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_computeXorOfAllElements_no2(){
        final long[][][] arrayToBeAnalyzed = { { { 1, 2, 0, 3 }, { 4, 5, 6 } }, { { 7, 8 }, { 9 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x1L,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_computeXorOfAllElements_no3(){
        final long[][][] arrayToBeAnalyzed = { { { 1 } }, { { 10, 11 }, { 12 }, { 13, 14, 15 }, { 16, 17, 18, 19, 20 } }, { { 2 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x16L,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_computeXorOfAllElements_no10(){
        final long[][][] arrayToBeAnalyzed = {{{}}};
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x0L,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_computeXorOfAllElements_no11(){
        final long[][][] arrayToBeAnalyzed = {{{}},{{},{},{7},{}},{{8},{},{},{},{1}},{{}}};
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xEL,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_computeXorOfAllElements_no90(){
        final long[][][] arrayToBeAnalyzed = { { { Character.MIN_VALUE, Long.MAX_VALUE, Byte.MIN_VALUE }, { Character.MAX_VALUE } }, { { Long.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Character.MIN_VALUE, Byte.MAX_VALUE, Byte.MIN_VALUE } }, { { Character.MAX_VALUE, } }, { { Integer.MAX_VALUE }, { Byte.MAX_VALUE, Integer.MIN_VALUE }, { Long.MIN_VALUE } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x8000000000000000L,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_computeXorOfAllElements_no99(){
        final long[][][] arrayToBeAnalyzed = null;
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x0L,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###    
    //###
    
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_computeXorOfAllElements_no1(){
        final long[][][] arrayToBeAnalyzed = { { { 1 } }, null, { { 2 } }, null, null, { null, { 3 }, null }, { { 4 }, null, { 5 }, null, { 6 } } };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0x7L,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "computeXorOfAllElements()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_computeXorOfAllElements_no2(){
        final long[][][] arrayToBeAnalyzed = { { { -1 }, {} }, {}, null, {}, {{}}, null, null, { null, {}, null }, { {}, null, {}, null, {}, {}, { 1 } }, {}, {}, };
        subTestBehavior_computeXorOfAllElements( arrayToBeAnalyzed,  0xFFFFFFFFFFFFFFFEL,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    private void subTestBehavior_computeXorOfAllElements(
        final long[][][] testParameter,                                         // test paramter - array that shall be analyzed
        final long expectedResult,                                              // expected result
        final int points,                                                       // points in case of success
        final TL testLevel,                                                     // test level
        final String testName                                                   // name of test method that is responsible fot the test
    ){
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final ArrayAnalyzer_I aa = (ArrayAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][][].class },  new Object[] { testParameter } ));
            assertEquals( expectedResult, aa.computeXorOfAllElements() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( testLevel, exercise, TC.BEHAVIOR ),  new TestResult( testName, points ) );
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   getCensus()
    //###
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    
    /** Einfacher Test: Funktion "getCensus()" aaa. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getCensus_aaa1(){
        final long[][][] arrayToBeAnalyzed = {{{1}}};
        final long[][] expectedResult = {{ 1, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "getCensus()" bbb. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getCensus_bbb1(){
        final long[][][] arrayToBeAnalyzed = { { { 1, 2 }, { 4, 5 } }, { { 6, 7 }, { 8, 9 } } };
        final long[][] expectedResult = {{ 1, 1 }, { 2, 1 }, { 4, 1 }, { 5, 1 }, { 6, 1 }, { 7, 1 }, { 8, 1 }, { 9, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    /** Einfacher Test: Funktion "getCensus()" bbb. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getCensus_bbb2(){
        final long[][][] arrayToBeAnalyzed = { { { 4, 2 }, { 7, 4 } }, { { 5, 5 }, { 4, 4 } } };
        final long[][] expectedResult = {{ 2, 1 }, { 4, 4 }, { 5, 2 }, { 7, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Einfacher Test: Funktion "getCensus()" bbb. */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_getCensus_bbb3(){
        final long[][][] arrayToBeAnalyzed = { { { 17, 13 }, { 17, 17 } }, { { 13, 17 }, { 17, 13 } } };
        final long[][] expectedResult = {{ 13, 3 }, { 17, 5 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.B,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
        
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getCensus_no1(){
        final long[][][] arrayToBeAnalyzed = { { { 1 }, { 2, 3 } }, { { 4, 5, 6 }, { 7, 8, 9, 10 } } };
        final long[][] expectedResult = {{ 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 5, 1 }, { 6, 1 }, { 7, 1 }, { 8, 1 }, { 9, 1 }, { 10, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getCensus_no2(){
        final long[][][] arrayToBeAnalyzed = { { { 1, 2, 0, 3 }, { 4, 5, 6 } }, { { 7, 8 }, { 9 } } };
        final long[][] expectedResult = {{ 0, 1 }, { 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 5, 1 }, { 6, 1 }, { 7, 1 }, { 8, 1 }, { 9, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getCensus_no3(){
        final long[][][] arrayToBeAnalyzed = { { { 1 } }, { { 10, 11 }, { 12 }, { 13, 14, 15 }, { 16, 17, 18, 19, 20 } }, { { 2 } } };
        final long[][] expectedResult = {{ 1, 1 }, { 2, 1 }, { 10, 1 }, { 11, 1 }, { 12, 1 }, { 13, 1 }, { 14, 1 }, { 15, 1 }, { 16, 1 }, { 17, 1 }, { 18, 1 }, { 19, 1 }, { 20, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getCensus_no10(){
        final long[][][] arrayToBeAnalyzed = {{{}}};
        final long[][] expectedResult = {{}};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getCensus_no11(){
        final long[][][] arrayToBeAnalyzed = {{{}},{{},{},{7},{}},{{8},{},{},{},{1}},{{}}};
        final long[][] expectedResult = {{ 1, 1 }, { 7, 1 }, { 8, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getCensus_no12(){
        final long[][][] arrayToBeAnalyzed = {{{}},{{},{42,42,42},{42},{}},{{42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42,42},{},{},{},{42}},{{}}};
        final long[][] expectedResult = {{ 42, 42 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method() 
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_getCensus_no90(){
        final long[][][] arrayToBeAnalyzed = { { { Character.MIN_VALUE, Long.MAX_VALUE, Byte.MIN_VALUE }, { Character.MAX_VALUE } }, { { Long.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Character.MIN_VALUE, Byte.MAX_VALUE, Byte.MIN_VALUE } }, { { Character.MAX_VALUE, } }, { { Integer.MAX_VALUE }, { Byte.MAX_VALUE, Integer.MIN_VALUE }, { Long.MIN_VALUE } } };
        final long[][] expectedResult = {{ Character.MIN_VALUE, 2 }, { Character.MAX_VALUE, 2 }, { Long.MAX_VALUE, 2 }, { Long.MIN_VALUE, 1 }, { Byte.MIN_VALUE, 2 }, { Byte.MAX_VALUE, 2 }, { Integer.MAX_VALUE, 2 }, { Integer.MIN_VALUE, 2 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.C,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###    
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_getCensus_no1(){
        final long[][][] arrayToBeAnalyzed = null;
        final long[][] expectedResult = {};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_getCensus_no2(){
        final long[][][] arrayToBeAnalyzed = { null };
        final long[][] expectedResult = {};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_getCensus_no3(){
        final long[][][] arrayToBeAnalyzed = { { null } };
        final long[][] expectedResult = {};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_getCensus_no4(){
        final long[][][] arrayToBeAnalyzed = { null, { null, {}, null }, null };
        final long[][] expectedResult = {};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_getCensus_no13(){
        final long[][][] arrayToBeAnalyzed = { null, { null, {}, null }, null, { null, { 13 }, null, {13}, null, { 13, 13, 13 }, { 17, 13 }, null, { 13 }, null }, null };
        final long[][] expectedResult = { { 13, 7 }, { 17, 1 } };
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_getCensus_no11(){
        final long[][][] arrayToBeAnalyzed = { { { 1 } }, null, { { 2 } }, null, null, { null, { 3 }, null }, { { 4 }, null, { 5 }, null, { 6 } } };
        final long[][] expectedResult = {{ 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 5, 1 }, { 6, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()
    
    /** Funktions-Test: "getCensus()". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_getCensus_no12(){
        final long[][][] arrayToBeAnalyzed = { { { -1 }, {} }, {}, null, {}, {{}}, null, null, { null, {}, null }, { {}, null, {}, null, {}, {}, { 1 }, { -1 } }, {}, {}, };
        final long[][] expectedResult = {{ -1, 2 }, { 1, 1 }};
        subTestBehavior_getCensus( arrayToBeAnalyzed,  expectedResult,  countsOnePoint,  TL.D,  new Object(){}.getClass().getEnclosingMethod().getName() );
    }//method()    
    
    
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    private void subTestBehavior_getCensus(
        final long[][][] testParameter,                                         // test paramter - array that shall be analyzed
        final long[][] expectedResult,                                          // (coded) expected result
        final int points,                                                       // points in case of success
        final TL testLevel,                                                     // test level
        final String testName                                                   // name of test method that is responsible fot the test
    ){
        final String requestedRefTypeName = "ArrayAnalyzer";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final ArrayAnalyzer_I aa = (ArrayAnalyzer_I)( TS.generateRequestedObject( requestedRefTypeWithPath,  new Class[]{ long[][][].class },  new Object[] { testParameter } ));
            final CensusElem_I[] actualResult = aa.getCensus();
            final Map<Long,Long> actualResultTable = new TreeMap<Long,Long>();
            for( CensusElem_I entry : actualResult ){
                actualResultTable.put( entry.getElem(), (long)( entry.getCount() ) );
            }//for
            final Map<Long,Long> expectedTable = new TreeMap<Long,Long>();
            for( final long[] tmp : expectedResult ){
                if( 2==tmp.length ){
                    expectedTable.put( tmp[0], tmp[1] );
                }else if( 0 != tmp.length ){
                    throw new IllegalStateException( String.format( "INTERNAL ERROR - this was NOT expected - call schaefers -> %d", tmp.length ));
                }//if
            }//for
            assertEquals( expectedTable, actualResultTable );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( testLevel, exercise, TC.BEHAVIOR ),  new TestResult( testName, points ) );
        }//if
    }//method()    
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()                                                 // BlueJ keeps state, otherwise
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 58;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A4;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evalution or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
  //final static private boolean enableDbgOutputAfterEachTest  =  ( 0 != ( dbgConfigurationVector & 0x0080_0000 ));
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
