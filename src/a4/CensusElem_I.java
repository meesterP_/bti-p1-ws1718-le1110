package a4;

/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Das Interface CensusElem_I
 * <ul>
 *     <li>beschreibt ein Zensus-Element und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und fordert die entsprechenden Methoden ein.</li>
 * </ul>
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public interface CensusElem_I {
    
    /**
     * Diese Methode soll den im Zensus-Element erfassten Grund-Werte aus dem (zuvor dem ArrayAnalyzer-Konstruktor ubergebenen) Array abliefern.
     * Leere bzw. nicht vorhandene Eintraege (im zuvor dem ArrayAnalyzer-Konstruktor uebergebenen Array) sollen NICHT erfasst werden.
     *
     * @return Wert des Zensus-Element erfassten Eintrags-Werts.
     */
    long getElem();
    
    /**
     * Diese Methode soll die Anzahl der Eintraege des zugehoerigen im Zensus-Element erfassten Grund-Werts
     * (aus dem zuvor dem ArrayAnalyzer-Konstruktor ubergebenen Array) abliefern.
     *
     * @return Anzahl der Eintaege des zugehoerigen im Zensus-Element erfassten Grund-Werts.
     */
    int getCount();
    
}//interface
