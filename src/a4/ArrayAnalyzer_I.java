package a4;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Das Interface ArraydAnalyzer_I
 * <ul>
 *     <li>beschreibt einen ArrayAnalyzer und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und fordert die entsprechenden Methoden ein.</li>
 * </ul>
 * Die von Ihnen zu implementierende Klasse ArrayAnalyzer muss
 * <ul>
 *     <li>einen oeffentlichen Konstruktor aufweisen, der der folgenden Signatur genuegt:<br />
 *         <code>ArrayAnalyzer( long[][][] )</code>
 *     </li>
 * </ul>
 * Das als Parameter uebergebene Array wird von den nachfolgend eingeforderten Methoden benoetigt,
 * da diese Methoden unterschiedliche Auswertungen des Array implementieren.<br />
 * Das Array muss <u>NICHT</u> gutartig sein. Es kann Luecken enthalten oder darf leer sein.
 * Jedes syntaktisch einwandfreie Array, das dem Typ long[][][] genuegt, ist zu akzeptieren
 * und von den nachfolgend eingeforderte Methoden zu verarbeiten.<br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse findet sich auf dem Aufgabenzettel.
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public interface ArrayAnalyzer_I {
    
    /**
     * Diese Methode soll eine Exklusiv-Oder-Verknuepfung aller im (zuvor dem Konstruktor uebergebenen) Array enthaltenen Grund-Werte durchfuehren.
     * (Hinweis: 0 ist das neutrale Element der Exklusiv-Oder-Verknuepfung).
     * Jedes im Array enthaltene Element ist genau einmal zu erfassen.
     * (Als Konsequenz eines leeren Array) nicht vorhandene Grund-Werte sollen als 0 gewertet werden.
     * Bemerkung: 0 ist das neutrale Element der Exklusiv-Oder-Verknuepfung.
     *
     * @return Ergebnis der Exklusiv-Oder-Verknuepfung aller Array-Elemente.
     */
    long computeXorOfAllElements();
    
    /**
     * Diese Methode soll zaehlen, wie oft ein Grund-Wert (vom Typ long) als Element im (zuvor dem Konstruktor uebergebenen) Array eingetragen ist.
     * Es sollen nur vorhandene Grund-Werte gezaehlt werden. Leere bzw. nicht vorhandene Eintraege sollen <u>NICHT</u> erfasst werden.
     *
     * @return (Ergebnis-)Array, dessen Elemente beschreiben, wie oft ein einzelener Grund-Wert (vom Typ long) im (zuvor dem Konstruktor uebergebenen) Array eingetragen ist.
     */
    CensusElem_I[] getCensus();
    
}//interface
