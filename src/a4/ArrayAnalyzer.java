package a4;

public class ArrayAnalyzer implements ArrayAnalyzer_I {
    private long[][][] array;

    public ArrayAnalyzer(long[][][] MDArray) {
        this.array = MDArray;
    }

    @Override
    public long computeXorOfAllElements() {
        long result = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[i][j].length; k++) {
                    result = result ^ array[i][j][k];
                }
            }
        }
        return result;
    }

    @Override
    public CensusElem_I[] getCensus() {
        // create array and fill it with CensusElem of every single digit number
        CensusElem[] ceArray = new CensusElem[10];
        for (int i = 0; i < ceArray.length; i++) {
            ceArray[i] = new CensusElem(array, i);
        }

        // create an array of the counters of the CensusElements and count how many are not 0
        int elemCountersNotZeroCounter = 0;
        int[] elemCounters = new int[ceArray.length];
        for (int i = 0; i < elemCounters.length; i++) {
            elemCounters[i] = ceArray[i].getCount();
            if (elemCounters[i] != 0) {
                elemCountersNotZeroCounter++;
            }
        }

        // create an array of CensusElem Objects and fill it with the ones where the count is not 0
        CensusElem_I[] gc = new CensusElem_I[elemCountersNotZeroCounter];
        for (int i = 0; i < ceArray.length; i++) {
            if (elemCounters[i] != 0) {
                gc[i] = ceArray[i];
            }
        }

        return gc;
    }

}
