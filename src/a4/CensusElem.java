package a4;

public class CensusElem implements CensusElem_I {
    // Attributes
    private long elem = -1;
    private long[][][] array;

    public CensusElem(long[][][] array, long elem) {
        this.array = array;
        this.elem = elem;
    }

    @Override
    public long getElem() {
        return elem;
    }

    @Override
    public int getCount() {
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                for (int k = 0; k < array[i][j].length; k++) {
                    if (array[i][j][k] == elem) {
                        result++;
                    }
                }
            }
        }
        return result;
    }

}
