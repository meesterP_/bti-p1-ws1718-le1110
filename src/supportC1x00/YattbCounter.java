package supportC1x00;


/**
 * Demo and Reference LabExam for P1<br />
 * <br />
 * YattbCounter ::= Yet Another Tribute To Bluej  COUNTER <br />
 * <br />
 * Resulting out of the BlueJ BUGs (e.g. not supporting @BeforeClass and @Afterlass)
 * a special counter is needed  counting the number of executed JUnit-tests.
 * This counter is named YattbCounter.
 * The YattbCounter is needed as workaround for the missing BlueJ @BeforeClass and @Afterlass support.
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class YattbCounter {
    
    /**
     * Determines if JUnit-test is the first test started.
     * Attention this alone does NOT prevent that anoter test has already passed by ;-)
     * The method waitUntilFirstHasFinished() is addressing this.
     *
     * @return true  if JUnit-test is the first test started
     */
    synchronized public boolean isFirstStarting(){
        if( startedCounter >= numberOfTests ){
            throw new IllegalStateException(
                String.format(
                    "Actually started #d != #d expected",
                    startedCounter,
                    numberOfTests
                )
            );
        }//if
        
        final boolean isFirst = ( 0 == startedCounter );
        startedCounter++;
        return isFirst;
    }//method()
    
    
    /**
     * Determines if JUnit-test is the last test started.
     * Attention this alone does NOT prevent that other tests dawdling around still need to do their job ;-)
     * ...
     *
     * @return true  if JUnit-test is the last test finishing
     */
    synchronized public boolean isLastFinishing(){
        if( owingCounter <= 0 ){
            throw new IllegalStateException(
                String.format(
                    "Owing:%d  =>  actually finished %d != %d expected",
                    owingCounter,
                    (numberOfTests - owingCounter),
                    numberOfTests
                )
            );
        }//if
        
        owingCounter--;
        final boolean isLast = ( 0 == owingCounter );
        if( isLast  &&  startedCounter!=numberOfTests ){
            throw new IllegalStateException(
                String.format(
                    "startedCounter=%d != %d=numberOfTests",
                    startedCounter,
                    numberOfTests
                )
            );
        }//if
        return isLast;
    }//method()
    
    
    /**
     * ...
     */
    synchronized public void waitUntilFirstHasFinished(){
        if( ! firstHasFinished ){
            try{
                wait();
            }catch( final InterruptedException ex ){
                throw new IllegalStateException( ex.getMessage() );             // Illegal State since, ??? who has send interrupt ???
            }//try
        }//if
    }//method()
    
    
    /**
     * ...
     */
    synchronized public void signalThatFirstHasFinished(){
        firstHasFinished = true;
        notifyAll();
    }//method()
    
    
    /**
     * ...
     */
    synchronized public void reset(){
        firstHasFinished = false;
        startedCounter = 0;
        owingCounter = numberOfTests;
    }//method()
    
    
    /**
     * ...
     *
     * @return  ...
     */
    synchronized public int getStartedCounter(){
        return startedCounter;
    }//method()
    
    /**
     * ...
     *
     * @return  ...
     */
    synchronized public int getOwingCounter(){
        return owingCounter;
    }//method()
    
    
    
    
    
    /**
     * ...
     *
     * @param numberOfTests  ...
     */
    public YattbCounter( final int numberOfTests ){
        if( 0 >= numberOfTests ) throw new IllegalArgumentException();
        
        this.numberOfTests = numberOfTests;
        firstHasFinished = false;
        startedCounter = 0;
        owingCounter = numberOfTests;
    }//constructor()
    
    
    
    
    
    private boolean firstHasFinished;
    private int startedCounter;
    private int owingCounter;
    
    final private int numberOfTests;
    
}//class
