package supportC1x00;


import java.util.HashMap;
import java.util.Map;


/**
 * Demo and Reference LabExam for P1<br />
 * <br />
 * Configuration
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class Configuration {
    
    /**
     * The debug-output enable vector controls the debug messages printed on screen
     */
    //                                                 vvvv_vvvv__vvvv_vvvv____vvvv_vvvv__vvvv_vvvv
    final static public int dbgConfigurationVector = 0b1000_0000__0000_0000____0000_0000__0000_0011;
    //                                                 +..._....__--.._..-b____...._..-?__...._.-?+
    //                                                 ^          ^^     ^^           ^^        ^^^
    //                                                 |          ||     ||           ||        |||
    //                       enable automatic test   --/          ||     ||           ||        |||
    //                                                            ||     ||           ||        |||
    //                    (temporary) debug output   -------------/|     ||           ||        |||
    //        wpi related compensation information   --------------/     ||           ||        |||
    //                                                                   ||           ||        |||
    //  support for BlueJ JUnit annotation bug(fix)  --------------------/|           ||        |||
    //              BlueJ JUnit annotation bug fix   ---------------------/           ||        |||
    //                                                                                ||        |||
    //        manual review of basic general stuff(*)---------------------------------/|        |||
    //            printExamineeInfoForManualReview(*)----------------------------------/        |||
    //                                                                                          |||
    //                       detailed result table   -------------------------------------------/||
    //                                result table   --------------------------------------------/|
    //                                         WPI   ---------------------------------------------/
    //
    //
    // (*) ATTENTION: As result of BlueJ-BUG, this influences the workaround: "numberOfTests" (in TestFrame)
    
    
    
    /**
     * Path to data base respectively name of directory containing test results without database file
     */
    final static String testResultDataBasePath = "TestResultDatabaseC1x00";     // package scope on purpose
    
    /**
     * (Base) name without extension
     * E.g. there will be ".data" and ".guard"
     */
    final static String testResultDataBaseFileBaseName =  "testResultDB";       // package scope on purpose
    
    
    /**
     * Version ID of configuration class/structure/possibilities
     */
    final static String configurationVersionID = "2016/12/31 v1.00";

    
    
    
    
    
    //--------------------------------------------------------------------------
    //
    // INTERNAL
    //
    
    /**
     * Table respectively map containing max. point that can be achieved.
     */
    final static Map<ValuationUnit,Integer> valuationTable = new HashMap<ValuationUnit,Integer>(){
        {   //---~~~~~~~~~~~~~~~~~~~VVVVVVVVVVV~~~~VVV~~~
            put( new ValuationUnit( TL.I, TE.A1 ),  10 );
            //------------------------------------------
            put( new ValuationUnit( TL.A, TE.A2 ),  10 );
            put( new ValuationUnit( TL.A, TE.A3 ),  11 );
            put( new ValuationUnit( TL.A, TE.A4 ),  10 );
            put( new ValuationUnit( TL.A, TE.A5 ),  10 );
            //------------------------------------------
            put( new ValuationUnit( TL.B, TE.A2 ),  11 );
            put( new ValuationUnit( TL.B, TE.A3 ),   5 );
            put( new ValuationUnit( TL.B, TE.A4 ),  24 );
            put( new ValuationUnit( TL.B, TE.A5 ),   7 );
            //------------------------------------------
            put( new ValuationUnit( TL.C, TE.A2 ),  12 );
            put( new ValuationUnit( TL.C, TE.A3 ),  52 );
            put( new ValuationUnit( TL.C, TE.A4 ),  15 );
            put( new ValuationUnit( TL.C, TE.A5 ),   2 );
            //------------------------------------------
            put( new ValuationUnit( TL.D, TE.A2 ),  14 );
            put( new ValuationUnit( TL.D, TE.A3 ),  22 );
            put( new ValuationUnit( TL.D, TE.A4 ),   9 );
            put( new ValuationUnit( TL.D, TE.A5 ),   2 );
        }//constructor()
        //
        final static private long serialVersionUID = CentralVersionData.centralTestResultDataBaseRelatedSerialVersionUID;   // __???___<161214>: Is this actually necessary ???
    };//class
    //
    /**
     * Number of levels per exercise (Attention there are different definitions of level - still cleanup required ;-)
     */
    //                                                                             ___1___  ___4____________  ___4____________  ___4____________  ___4____________
    final static int[] ratioVector = { 1, 4, 4, 4, 4 };                         // TL.I:A1, TL.A:A2,A3,A4,A5, TL.B:A2,A3,A4,A5, TL.C:A2,A3,A4,A5, TL.D:A2,A3,A4,A5  used in TestResultTable.printPerformance()
    
}//class
