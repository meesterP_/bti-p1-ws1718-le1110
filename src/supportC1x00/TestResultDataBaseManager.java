package supportC1x00;


import static supportC1x00.Configuration.testResultDataBaseFileBaseName;
import static supportC1x00.Configuration.testResultDataBasePath;
import static supportC1x00.Herald.Medium.*;
//
//
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
//
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
//
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;


/**
 * Demo and Reference LabExam for P1<br />
 * <br />
 * The Test Result DataBase Manager is expected to be created for each individual exercise.
 * Hence it is NOT in the responsibility of the Test Result DataBase Manager to handle two or more exercises at the same time!
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class TestResultDataBaseManager {
    
    /**
     * This method resets the current test result table.
     *
     */
    synchronized public void reset(){
        currentTestResults.clear();
    }//method()
    
    
    
    /**
     * This method enters the current test result locally into the current test result table.
     * Later on ( @AfterClass inside runTestTearDownAfterAllUnitTestsHaveFinished() of related test frame ) it is entered into the data base.
     *
     * @param testTopic         the very current test topic
     * @param testResult        the current test result
     */
    synchronized public void enterLocally(
        final TestTopic  testTopic,                                             // the very current test topic
        final TestResult  testResult                                            // the current test result
    ){
        // check for parameter consistency/meaningfulness
        final TL testLevel = testTopic.getLevel();
        final TC testCategory = testTopic.getCategory();
        final String testName = testResult.getTestMethodName();
        final int testWeight = testResult.getWeight();
        //
        /*
        // Just in case zero pints are supported for TC.DBGPRINT related tests, accept them ;-)
        if(     (( 0 >= testWeight ) && ( TC.DBGPRINT != testCategory ))
            ||  (  0 >  testWeight )
        ){
        */
        if( 0 >= testWeight ){
            throw new IllegalArgumentException( String.format( 
                "SETUP ERROR: invalid test weight -> method:%s, weight:%d, category:%s\n",
                testName,
                testWeight,
                testCategory
            ));
        }//if
        //\=> parameter are valid respectively NOT null
        //
        // check for consistency:  test name <-> test level
        switch( testLevel ){
            case I: {
                    final Pattern pattern = Pattern.compile( "^tol_0i_[a-z].*$" );
                    final Matcher matcher = pattern.matcher( testName );
                    if( ! matcher.matches() ){
                        Herald.proclaimMessage( SYS_ERR,  String.format( "[%s]:  tol_0i_ <-> %s",  testLevel, testName ));
                        throw new IllegalStateException( "Internal coding error - did not expect to end up \"here\" - call Michael Schaefers" );
                    }//if
                }//scope
              break;
            case A: {
                    final Pattern pattern = Pattern.compile( "^tol_1e_[a-z].*$" );
                    final Matcher matcher = pattern.matcher( testName );
                    if( ! matcher.matches() ){
                        Herald.proclaimMessage( SYS_ERR,  String.format( "[%s]:  tol_1e_ <-> %s",  testLevel, testName ));
                        throw new IllegalStateException( "Internal coding error - did not expect to end up \"here\" - call Michael Schaefers" );
                    }//if
                }//scope
              break;
            case B: {
                    final Pattern pattern = Pattern.compile( "^tol_2b_[a-z].*$" );
                    final Matcher matcher = pattern.matcher( testName );
                    if( ! matcher.matches() ){
                        Herald.proclaimMessage( SYS_ERR,  String.format( "[%s]:  tol_2b_ <-> %s",  testLevel, testName ));
                        throw new IllegalStateException( "Internal coding error - did not expect to end up \"here\" - call Michael Schaefers" );
                    }//if
                }//scope
              break;
            case C: {
                    final Pattern pattern = Pattern.compile( "^tol_3n_[a-z].*$" );
                    final Matcher matcher = pattern.matcher( testName );
                    if( ! matcher.matches() ){
                        Herald.proclaimMessage( SYS_ERR,  String.format( "[%s]:  tol_3n_ <-> %s",  testLevel, testName ));
                        throw new IllegalStateException( "Internal coding error - did not expect to end up \"here\" - call Michael Schaefers" );
                    }//if
                }//scope
              break;
            case D: {
                    final Pattern pattern = Pattern.compile( "^tol_4s_[a-z].*$" );
                    final Matcher matcher = pattern.matcher( testName );
                    if( ! matcher.matches() ){
                        Herald.proclaimMessage( SYS_ERR, String.format( "[%s]:  tol_4s_ <-> %s",  testLevel, testName ));
                        throw new IllegalStateException( "Internal coding error - did not expect to end up \"here\" - call Michael Schaefers" );
                    }//if
                }//scope
              break;
            default: {
                    Herald.proclaimMessage( SYS_ERR,  String.format( "[%s]:  Did not expect to end up \"here\": %s",  testLevel, testName ));
                    if( true )  throw new IllegalStateException( "Internal coding error - call Michael Schaefers" );
                }//scope
              break;
        }//switch
        //\=> parameter are consistent respectively meaningful
        
        // start of action
        currentTestResults.enterTestResult( testTopic, testResult );
    }//method()
    
    
    
    
    
    /**
     * This method (removes first the old/obsolete test results and)
     * inserts the current test result(s) into the test result data base.
     * The test results have to be related to the current exercise.
     * This is checked.
     */
    synchronized public void updateSynchronizedExerciseRelatedResults(){
        // initial check if "situation" is as expected
        for( TestTopic testTopic : currentTestResults.keySet() ){
            final TE testExercise = testTopic.getExercise();
            if( testExercise != theExercise ){
                final Set<TestResult> testResults = currentTestResults.get( testTopic );
                for( final TestResult testResult : testResults ){               // first one will throw exception (but it's a set, so what? ;-)
                    final String testName = testResult.getTestMethodName();
                    throw new IllegalStateException( String.format(
                        "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> result is NOT expected inside test result table -> method:%s  exercise under test:%s <-> %s:exercise related to found result",
                        testName,
                        theExercise,
                        testExercise
                    ));
                }//for
            }//if
        }//for
        //\=> current test results contain only results related to exercise under test
        
        
        //"get theTable from data base"
        RandomAccessFile testResultDataBaseGuard = null;                        // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        FileChannel channel = null;                                             // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        FileLock fileLock = null;                                               // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        try/*(                                                                  // BlueJ BUG - AutoClosable is NOT supported
            final RandomAccessFile testResultDataBaseGuard = new RandomAccessFile( testResultDataBaseGuardPath.toString(), "rw" );//AutoClosable is NOT supported
            final FileChannel channel = testResultDataBaseGuard.getChannel();                            // BlueJ BUG - AutoClosable is NOT supported
            final FileLock fileLock = channel.lock();                           // <<<<==== GUARD        // BlueJ BUG - AutoClosable is NOT supported
        )*/{//##################################################################
            //####  GUARDED - GUARDED - GUARDED - GUARDED - GUARDED - GUARDED
            //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
            
            testResultDataBaseGuard = new RandomAccessFile( testResultDataBaseGuard_Path.toString(), "rw" ); // BluJ-BUG - AutoClosable is NOT supported
            channel = testResultDataBaseGuard.getChannel();                     // WorkAround for BlueJ BUG - AutoClosable is NOT supported
            fileLock = channel.lock();                                          // WorkAround for BlueJ BUG - AutoClosable is NOT supported
            //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
            
            final boolean requestedDataBaseExists = Files.exists( testResultDataBase_Path, new LinkOption[]{ LinkOption.NOFOLLOW_LINKS } );
            TestResultTable allTestResults;
            if( requestedDataBaseExists ){
                //=> data base exists ;-)
                
                TestResultTable oldStoredTable = iLoadTestResultTableFromDataBaseFile( fileLock );
                oldStoredTable.removeExcercise( theExercise );                  // remove exercise related entries from old stored table
                if( null != currentTestResults ){
                    currentTestResults.integrateTestResults( oldStoredTable );
                    allTestResults = currentTestResults;
                }else{
                    allTestResults = oldStoredTable;
                }//if
            }else{
                allTestResults = currentTestResults;
            }//if
            if( null != allTestResults ){
                iStoreTestResultTableIntoDataBaseFile( fileLock, allTestResults );
            }else{
                System.out.printf( "NO results at all\n" );
            }//if
            
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            //####  GUARDED - GUARDED - GUARDED - GUARDED - GUARDED - GUARDED
            //##################################################################
        }catch( final IOException ex ){                                         // FileNotFoundException | IOException
            ex.printStackTrace();
            throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"\n",  ex.getMessage() ));
        }finally{                                                               // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            try{                                                                // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                fileLock.close();                                               // including .release() ;-)        && - AutoClosable is NOT supported
                channel.close();                                                // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                testResultDataBaseGuard.close();                                // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            }catch( final IOException ex ){                                     // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                ex.printStackTrace();                                           // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"\n",  ex.getMessage() ));
            }//try                                                              // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        }//try                                                                  // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
    }//method()
    
    
    
    /**
     * This method inserts the current test result(s) into the test result data base.
     * The test results have to be related to the current exercise.
     * This is checked.
     */
    synchronized public TestResultTable loadSynchronizedExerciseRelatedResults(){
        TestResultTable theTable = null;
        
        RandomAccessFile testResultDataBaseGuard = null;                        // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        FileChannel channel = null;                                             // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        FileLock fileLock = null;                                               // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        try/*(                                                                  // BlueJ BUG - AutoClosable is NOT supported
            final RandomAccessFile testResultDataBaseGuard = new RandomAccessFile( testResultDataBaseGuardPath.toString(), "rw" );//AutoClosable is NOT supported
            final FileChannel channel = testResultDataBaseGuard.getChannel();                            // BlueJ BUG - AutoClosable is NOT supported
            final FileLock fileLock = channel.lock();                           // <<<<==== GUARD        // BlueJ BUG - AutoClosable is NOT supported
        )*/{//##################################################################
            //####  GUARDED - GUARDED - GUARDED - GUARDED - GUARDED - GUARDED
            //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
            
            testResultDataBaseGuard = new RandomAccessFile( testResultDataBaseGuard_Path.toString(), "rw" ); // BluJ-BUG - AutoClosable is NOT supported
            channel = testResultDataBaseGuard.getChannel();                     // WorkAround for BlueJ BUG - AutoClosable is NOT supported
            fileLock = channel.lock();                                          // WorkAround for BlueJ BUG - AutoClosable is NOT supported
            //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
            
            final boolean requestedDataBaseExists = Files.exists( testResultDataBase_Path, new LinkOption[]{ LinkOption.NOFOLLOW_LINKS } );
            if( requestedDataBaseExists ){
                theTable = iLoadTestResultTableFromDataBaseFile( fileLock );
            }//if
            
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            //####  GUARDED - GUARDED - GUARDED - GUARDED - GUARDED - GUARDED
            //##################################################################
        }catch( final IOException ex ){                                         // FileNotFoundException | IOException
            ex.printStackTrace();
            throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"\n",  ex.getMessage() ));
        }finally{                                                               // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            try{                                                                // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                fileLock.close();                                               // including .release() ;-)        && - AutoClosable is NOT supported
                channel.close();                                                // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                testResultDataBaseGuard.close();                                // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            }catch( final IOException ex ){                                     // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                ex.printStackTrace();                                           // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"\n",  ex.getMessage() ));
            }//try                                                              // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        }//try                                                                  // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        
        return theTable;
    }//method()
    
    
    
    private TestResultTable iLoadTestResultTableFromDataBaseFile( final FileLock fileLock ){
        TestResultTable readTestResultTable = null;
        ObjectInputStream ois = null;                                   // WorkAround for BlueJ BUG - AutoClosable is NOT supported
        try/*(                                                          // BlueJ BUG - AutoClosable is NOT supported
            final ObjectInputStream ois = new ObjectInputStream( Files.newInputStream( testResultDatabasePath ));
        )*/{
            ois = new ObjectInputStream( Files.newInputStream( testResultDataBase_Path ));   // BBUG - AutoClosable is NOT supported
            readTestResultTable = (TestResultTable)( ois.readObject() );   // the "old" table currently stored in data base
        }catch( final ClassNotFoundException|IOException ex ){          // ClassNotFoundException | FileNotFoundException | IOException
            ex.printStackTrace();
            throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"", ex.getMessage() ));
        }finally{                                                       // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            try{                                                        // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                ois.close();                                            // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            }catch( final IOException ex ){                             // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                ex.printStackTrace();                                   // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"\n",  ex.getMessage() ));
            }//try                                                      // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        }//try                                                          // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        return readTestResultTable;
    }//method()
    
    private void iStoreTestResultTableIntoDataBaseFile( final FileLock fileLock,  final TestResultTable theTable ){
        ObjectOutputStream oos = null;                                      // result of  WorkAround for BlueJ BUG - AutoClosable is NOT supported
        try/*(                                                                                        // BlueJ BUG - AutoClosable is NOT supported
            final ObjectOutputStream oos = new ObjectOutputStream( Files.newOutputStream( testResultDatabasePath ));
        )*/{
            oos = new ObjectOutputStream( Files.newOutputStream( testResultDataBase_Path ));          // BLueJ BUG - AutoClosable is NOT supported
            oos.writeObject( theTable );
        }catch( final IOException ex ){                                     // FileNotFoundException | IOException
            ex.printStackTrace();
            throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"",  ex.getMessage() ));
        }finally{                                                           // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            try{                                                            // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                oos.close();                                                // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            }catch( final IOException ex ){                                 // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                ex.printStackTrace();                                       // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"\n",  ex.getMessage() ));
            }//try                                                          // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
        }//try 
    }//method()
    
     
    
    
    
    /**
    * ...
    *
    * @param currentExercise  ...
    */
    public TestResultDataBaseManager(                                           // package scope on purpose
        final TE currentExercise                                                // the current exercise
    ){
        if( ! guaranteeDataBaseExistence() ){
            throw new IllegalStateException( "can NOT generate test result dabase" );
        }//if
        theExercise = currentExercise;
        currentTestResults = TestResultTable.createEmptyTable();
    }//constructor()
    //
    private boolean guaranteeDataBaseExistence(){
        try{
            // check if requested directory exists - otherwise generate it
            final File resultDB = new File( testResultDataBasePath );
            if( resultDB.exists() ){
                if( ! resultDB.isDirectory() ){
                    throw new TestSupportException( "can NOT generate test result dabase" );
                }//if
            }else{
                final boolean directoryGenerationProblem = !resultDB.mkdir();
                if( directoryGenerationProblem ){
                    throw new TestSupportException( "can NOT generate test result dabase" );
                }//if
            }//if
            //=> directory is existent
            
        }catch( final TestSupportException ex ){
            ex.printStackTrace();
            return false;
        }//try
        return true;
    }//method()  
    
    
    
    
    
    /**
     * ...
     */
    final TestResultTable  currentTestResults;                                  // package scope on purpose
    
    final private TE  theExercise;                                              // ...
    
    
    final static private Path examRootPath = Paths.get( "." );
    final static private String localPathToDbGuard  = testResultDataBasePath+File.separator+testResultDataBaseFileBaseName+".guard";
    final static private String localPathToDatabase = testResultDataBasePath+File.separator+testResultDataBaseFileBaseName+".data";
    //
    final static private Path absoluteExamRootPath = examRootPath.toAbsolutePath();
    final static private Path normalizedAbsoluteExamRootPath = absoluteExamRootPath.normalize();
    final static private Path testResultDataBaseGuard_Path = Paths.get( normalizedAbsoluteExamRootPath.toString(), localPathToDbGuard );
    final static private Path testResultDataBase_Path      = Paths.get( normalizedAbsoluteExamRootPath.toString(), localPathToDatabase );
    
}//class

