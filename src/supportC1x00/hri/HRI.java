package supportC1x00.hri;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cards.Card;
import cards.Card.Rank;


// HRI ::= Hidden Reference Implementation(s) supporting test(s)
// Just capsulate "stuff" that shall NOT be shown as source text in lab exam
/**
 * LabExam P1<br />
 * <br />
 * ...
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExamDemo_4P1_162v02_170101_v03 (=LabExamDemo_4P1_162v02_161231_v03)
 */
public class HRI {
    
    // RCP ::= Reference CardProcessor 
    static public class RCP implements a5.CardProcessor_I {                               // package scope on purpose
        
        @Override
        public a5.Trips_I process( final Card card ){
            final Rank currentRank = card.getRank(); 
            a5.Trips_I trips = null;
            if ( ! map.containsKey( currentRank )){
                final List<Card> list = new ArrayList<>();                      // list chosen, since "doubles" supported and keeping order
                list.add( card );
                map.put( currentRank, list );
            }else{
                final List<Card> list = map.get( currentRank );
                list.add( card );
                if ( list.size() >= 3 ){                                        // "HERE": ">=" <=> "=="
                    class T implements a5.Trips_I {
                        @Override public Card get( final int no ){ return trips.get( no ); }
                        private T( final List<Card> list ){
                            if( 3 != list.size() )  throw new IllegalArgumentException( String.format( "INTERNAL ERROR: Invalid trip size ->  %d  ( != 3 )",  list.size() ));
                            trips.addAll( list );
                        }//constructor()
                        private final List<Card> trips = new ArrayList<>();
                    }//class
                    trips = new T( list );                                      // get trips (as result) and
                    list.clear();                                               // ..remove trips from list, since each card exclusively belongs to single trips
                }//if
            }//if
            return trips;
        }//method()
        
        @Override
        public void reset(){
            map.clear();
        }//method()
        
        
        public RCP(){
            map = new HashMap<Rank, List<Card>>();
        }//constructor()
        
        
        final private Map<Rank, List<Card>> map;
        
    }//class
    
}//class
