package supportC1x00;


import javafx.application.Platform;
//
import javafx.embed.swing.JFXPanel;
//
import javafx.scene.control.Label;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.Font;
//
import javafx.stage.Stage;


/**
 * Demo and Reference LabExam for P1<br />
 * <br />
 * Herald sent by you (the programmer) to convey messages or proclamations to the user ;-)
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class Herald {
    
    /**
     * Proclaim test count via given medium
     *
     * param medium  Proclamation is done via THIS VERY MEDIUM,
     * param cnt     test count
     */
    static public void proclaimTestCount (
        final Medium  medium,
        final int  cnt
    ){
        final String msg = String.format( "\n[>>>> @test# %d <<<<]\n", cnt );
        Herald.proclaimMessage( medium, msg );
    }//method()
    
    
    
    /**
     * Proclaim message via given medium
     *
     * param medium  Proclamation is done via THIS VERY MEDIUM,
     * param text    message to be printed
     */
    static public void proclaimMessage (
        final Medium  medium,
        final StringBuffer  text
    ){
        proclaimMessage( medium, text.toString() );
    }//method()
    
    /**
     * Proclaim message via given medium
     *
     * param medium  Proclamation is done via THIS VERY MEDIUM,
     * param text    message to be printed
     */
    static public void proclaimMessage (
        final Medium  medium,
        final StringBuilder text
    ){
        proclaimMessage( medium, text.toString() );
    }//method()
    
    /**
     * Proclaim message via given medium
     *
     * param medium  Proclamation is done via THIS VERY MEDIUM,
     * param text    message to be printed
     */
    static public void proclaimMessage (
        final Medium  medium,
        final String  text
    ){
        synchronized( staticState ){
            switch( medium ){
                case SYS_ERR:
                    // error message to programmer - this SHALL NOT happen during lab exam
                    System.out.flush();
                    System.err.flush();
                    System.err.println( text );
                    System.err.flush();
                  break;
                case SYS_OUT:
                    // some kind of information
                    System.err.flush();
                    System.out.flush();
                    System.out.println( text );
                    System.out.flush();
                  break;
                default:
                    // some kind of information
                    /*
                     * Currently unsupported (most of the time ;-),
                     * since there seems to be the danger of zombie threads.
                     * Especially with JavaFX the question is still open,
                     * how to start the GUI from a foreign (static) method and
                     * to tear it down in a propper way after BlueJ finishes.
                     */
                    if( false )  throw new UnsupportedOperationException();     // "GUARD" - the whole thing is pretty suspect
                    staticState.proclaimViaWindowFX( text );
                  break;
            }//switch
        }//synchronized
    }//method()
    
    
    private void proclaimViaWindowFX( final String message ){
        // Initialises JavaFX:
        new JFXPanel();
        // Makes sure JavaFX doesn't exit when first window is closed:
        Platform.setImplicitExit(false);
        // Runs initialisation on the JavaFX thread:
        Platform.runLater( () -> initialiseGUI( message ) );
    }//method()
    //
    private void initialiseGUI( final String message ){
        Stage stage = new Stage();
        final Label label = new Label( message );
        label.setFont( new Font( 24 ));
        final Group root = new Group( label );
        final Scene scene = new Scene( root );
        stage.setScene( scene );
        stage.setTitle( "ATTENTION:" );
        stage.show();                                       //=>stage.setVisible(true);
    }//method()
    
    
    
    
    
    // state containing:
    //  -)  Lock guarding that only one message is printed at time
    //  -)  JavaFX specific stuff (result of trial&error - further investigation required)
    static final private Herald staticState = new Herald();
    
    
    
    
    
    /**
     * Medium respectively chanel over that provlamation is done
     */
    static public enum Medium {
        SYS_ERR,
        SYS_OUT,
        WINDOW
    }//enum
    
}//class
