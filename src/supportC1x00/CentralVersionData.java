package supportC1x00;


/**
 * Demo and Reference LabExam for P1<br />
 * <br />
 * Central "container" for version IDs.
 * Here the version IDs are "greped" from the classes there are defined in.
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class CentralVersionData {
    
    // lab exam version ID
    public final static String centralLabExamVersionID = "2017/02/28 P1 LabExam1110 demo and reference";
    
    
    // test support routine collection (the "engine" ;-) version ID  - generally the "Demo and Reference lab exam" shall contain the latest version
    public final static String centralTestSupportVersionID = "2017/02/28 v1.10";
    
    
    // version ID of WPI computer
    public final static String centralWPIComputerVersionID = TestResultAnalyzer.wpiComputerVersionID;
    
    
    // version ID of configuration class/structure/possibilities
    public final static String centralTestConfigurationVersionID = Configuration.configurationVersionID;
    
    
    
    // "serialVersionUID": test result data base (format for serialization) version ID   - generally the "Demo and Reference lab exam" shall contain the latest version
    public final static long centralTestResultDataBaseRelatedSerialVersionUID = 2016_12_31_0002L;
    
}//class
