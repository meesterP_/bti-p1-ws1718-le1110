package a2;

public class Stack implements Stack_I {
    // Attributes
    private SingleLinkNode top;

    // Constructor
    public Stack() {
        top = null;
    }

    // Methods
    @Override
    public void push(Data data) {
        assert data != null : "illegal parameter!";
        SingleLinkNode newNode = new SingleLinkNode(data);
        if (!isEmpty()) {
            newNode.next = top;
        }
        top = newNode;
    }

    @Override
    public Data pop() {
        if (top == null) return null;
        Data returnInfo = top.info;
        top = top.next;
        return returnInfo;
    }

    @Override
    public Data top() {
        if (top == null) return null;
        return top.info;
    }

    @Override
    public void clear() {
        top = null;
    }

    @Override
    public boolean isEmpty() {
        return top == null;
    }

}
