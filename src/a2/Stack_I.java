package a2;

 
/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Das Interface Stack_I
 * <ul>
 *     <li>beschreibt einen Stack, der Datensaetze vom Typ Data aufnehmen kann und</li>
 *     <li>definiert die Funktionalitaet moeglicher Implementierungen und fordert die entsprechenden Methoden ein.</li>
 * </ul>
 * Die von Ihnen zu implementierende Klasse Stack muss
 * <ul>
 *     <li>sich wie ein Stack verhalten. Dies war Thema der Programmierenvorlesung.<br />
 *         Stichwort: LIFO.
 *     </li>
 *     <li>einen oeffentlichen Konstruktor aufweisen, der der folgenden Signatur genuegt:<br />
 *         <code>Stack()</code>
 *     </li>
 *     <li>eine selbstgeschriebene dynamische Datenstruktur sein.
 *         Sie duerfen fuer die Implementierung von Stack insbesondere NICHT auf die Collections zurueckgreifen.
 *         Die Implementierung muss in Form einer verketteten Liste erfolgen.
 *         Sie sollen die mitausgelieferten Klassen Data und Node fuer Ihre Implementierung nutzen.
 *      </li>
 * </ul>
 * Bemerkung:<br />
 * Noch einmal: Fuer diese Aufgabe duerfen Sie NICHT auf die Collections zurueckgreifen.
 * Sie muessen den Stack mit <strong>eigenen Mitteln</strong> selbst implementieren
 * und zwar als <strong>dynamische Datenstruktur</strong> bzw. konkret als verkettete Liste.<br />
 * Um Sie von Generics zu entlasten wurde auf die Klasse Data zurueckgegriffen,
 * die Sie auch schon aus der Programmieren-Vorlesung als Platzhalter-Klasse kennen
 * (und die hier die Rolle des Generics einnimmt).<br />
 * <br />
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse findet sich auf dem Aufgabenzettel.<br />
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
interface Stack_I {
    
    /**
     * Diese Methode legt einen Datensatz (data) auf dem Stack ab.
     * Diese Methode beachtet die LIFO-Ordnung.
     * Falls der uebergebene Parameter keine gueltige Referenz enthaelt, ist geeignet zu reagieren.
     * <br />
     * @param data bestimmt den Datensatz, der auf dem Stack abgelegt werden soll.
     */
    void push( final Data data );
    
    /**
     * Diese Methode liefert (von den im Stack enthaltenen Datensaetzen) den zuletzt
     * auf dem Stack abgelegten Datensatz und entfernt ihn vom Stack. Diese Methode
     * beachtet die LIFO-Ordnung.
     * <br />
     * @return Der zuletzt auf dem Stack abgelegten Datensatz.
     */
    Data pop();
    
    /**
     * Diese Methode liefert (von den im Stack enthaltenen Datensaetzen) den zuletzt
     * auf dem Stack abgelegten Datensatz. (Im Gegensatz zu pop() entfernt diese
     * Methode NICHT den Datensatz. Der Datensatz verbleibt im Stack.)
     * <br />
     * @return Der zuletzt auf dem Stack abgelegten Datensatz.
     */
    Data top();
    
    /**
     * Diese Methode leert den Stack.
     */
    void clear();
    
    /**
     * Diese Methode prueft, ob der Stack leer ist.<br />
     * @return <code>true</code> falls der Stack leer ist und
     *         sonst <code>false</code>.
     */
    boolean isEmpty();
    
}//interface
