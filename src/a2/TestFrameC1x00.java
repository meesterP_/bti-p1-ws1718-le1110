package a2;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.lang.reflect.Modifier;
//
import java.util.EmptyStackException;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben
 * und Ihnen nur eine Idee geben, wie gut Sie die Aufgabe geloest haben.<br />
 * Es ist insbesondere NICHT geplant, dass Sie alle Tests verstehen. Vermutlich gibt es so viele Tests,
 * dass Sie gar nicht die Zeit haben sich in alle Tests einzuarbeiten.<br />
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann NICHT als Beweis dienen,
 * dass der Code fehlerfrei ist. Es liegt in Ihrer Verantwortung sicher zu stellen,
 * dass Sie fehlerfreien Code geschrieben haben.<br />
 * Bei der Bewertung werden u.U. andere - konkret : modifizierte und haertere Tests - verwendet.
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 * 
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class TestFrameC1x00 {
    
    //##########################################################################
    //###
    //###   A / "1e"
    //###
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet  ?=>? 0 Punkte?). */
    @Test( timeout = commonLimit )
    public void tol_1e_printSupportForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0200 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );    
            
            final String requestedRefTypeName = "Stack";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;        
            try{
                
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
                TS.printDetailedInfoAboutObject( stack, "stack" );
                //
                if( TS.isActualMethod( stack.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  stack.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  stack.getClass().getSimpleName() );
                }//if
                
                System.out.printf( "\n\n" );
                System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
                System.out.printf( "\n\n" );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }finally{
                System.out.flush();
            }//try
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Existenz-Test: "Stack". */
    @Test( timeout = commonLimit )
    public void tol_1e_classExistence_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            // NO crash yet => success ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Test einiger Eigenschaften des Referenz-Typs "Stack". */
    @Test( timeout = commonLimit )
    public void tol_1e_properties_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue(                                     TS.isClass(             classUnderTest ));                                                                                      // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing(      classUnderTest, "Stack_I" ));
            assertTrue( "requested constructor missing",    TS.isConstructor(       classUnderTest, null ));
            assertTrue( "false constructor access modifier",TS.isAccessModifierSet( classUnderTest, null, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Stack" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesMethods_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "clear",   void.class,    null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "isEmpty", boolean.class, null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "top",     Data.class,    null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "pop",     Data.class,    null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "push",    void.class,    new Class<?>[]{ Data.class } ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "clear",   void.class,    null,                         Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "isEmpty", boolean.class, null,                         Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "top",     Data.class,    null,                         Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "pop",     Data.class,    null,                         Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "push",    void.class,    new Class<?>[]{ Data.class }, Modifier.PUBLIC )); // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Stack" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_propertiesFields_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Stack" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationMethods_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "Stack" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_1e_notationFields_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Grundsaetzlicher Test: Stack erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_1e_objectCreation_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "isEmpty()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_isEmpty(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            stack.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "push()". */
    @Test( timeout = commonLimit )
    public void tol_1e_behavior_push(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            stack.push( anyData );
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.A, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   B / "2b"
    //###
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "top()" ( und zuvor "push()" ). */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePushTop(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            Data justTakeData;
            stack.push( anyData );
            justTakeData = stack.top();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "pop()" ( und zuvor "push()" ). */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePushPop(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            Data justTakeData;
            stack.push( anyData );
            justTakeData = stack.pop();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfach(st)er Test: (NUR-)Methoden-Aufruf "clear()" ( und zuvor "push()" ). */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_clear(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            stack.push( anyData );
            stack.clear();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Einfacher Test: (NUR-)Sequenz-Methoden-Aufruf: "isEmpty()", "push()", "pop()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = stack.isEmpty();
            stack.push( anyData );
            justTakeData = stack.pop();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: (NUR-)Sequenz-Methoden-Aufruf: "isEmpty()", "push()", "pop()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = stack.isEmpty();
            stack.push( anyData );
            justTakeBool = stack.isEmpty();
            justTakeData = stack.pop();
            justTakeBool = stack.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Einfacher Test: (NUR-)Sequenz-Methoden-Aufruf: "isEmpty()", "push()", "top()", "pop()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = stack.isEmpty();
            stack.push( anyData );
            justTakeData = stack.top();
            justTakeData = stack.pop();
            justTakeBool = stack.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test: (NUR-)Sequenz-Methoden-Aufruf: "isEmpty()", "push()", "top()", "pop()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no4(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = stack.isEmpty();
            stack.push( anyData );
            justTakeBool = stack.isEmpty();
            justTakeData = stack.top();
            justTakeBool = stack.isEmpty();
            justTakeData = stack.pop();
            justTakeBool = stack.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test: (NUR-)Sequenz-Methoden-Aufruf: "clear()", "push()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no5(){
         final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
         
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
         try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            //
            stack.clear();
            stack.push( anyData );
            stack.clear();
            stack.push( anyData );
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test: (NUR-)Sequenz-Methoden-Aufruf "clear()", "isEmpty()", "pop()", "push()", "top()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_sequencePureMethodCall_no6(){
         final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
         
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 42 );
            boolean justTakeBool;
            Data justTakeData;
            //
            justTakeBool = stack.isEmpty();
            stack.clear();
            justTakeBool = stack.isEmpty();
            stack.push( anyData );
            justTakeBool = stack.isEmpty();
            justTakeData = stack.top();
            justTakeBool = stack.isEmpty();
            justTakeData = stack.pop();
            justTakeBool = stack.isEmpty();
            stack.clear();
            justTakeBool = stack.isEmpty();
            stack.push( anyData );
            justTakeBool = stack.isEmpty();
            justTakeData = stack.pop();
            justTakeBool = stack.isEmpty();
            stack.clear();
            justTakeBool = stack.isEmpty();
            justTakeBool = stack.isEmpty();
            stack.push( anyData );
            justTakeBool = stack.isEmpty();
            // NO crash yet => success ;-)
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if 
    }//method()
    
    
    /** Funktions-Test:"isEmpty()". */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isEmpty_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertTrue( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test:"isEmpty()" ( und zuvor "push()" ). */
    @Test( timeout = commonLimit )
    public void tol_2b_behavior_isEmpty_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 1 );
            stack.push( anyData );
            assertFalse( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.B, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   C / "3n"
    //###
    
    /** Funktions-Test: "isEmpty()" ( und zuvor "push()" ). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isEmpty_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertTrue( stack.isEmpty() );
            //
            final Data anyData = new Data( Integer.MIN_VALUE );
            stack.push( anyData );
            assertFalse( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "isEmpty()" ( und zuvor "push()", "pop()" ). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isEmpty_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertTrue( stack.isEmpty() );
            //
            final Data anyData = new Data( 13 );
            stack.push( anyData );
            assertFalse( stack.isEmpty() );
            //
            Data justTakeData = stack.pop();
            assertTrue( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if    
    }//method()
    
    /** Funktions-Test: "isEmpty()" ( und zuvor "push()", "top()" & "pop()" ). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_isEmpty_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            assertTrue( stack.isEmpty() );
            //
            final Data someData = new Data( 37 );
            Data justTakeData;
            stack.push( someData );
            assertFalse( stack.isEmpty() );
            //
            assertTrue( someData == stack.top() );
            assertFalse( stack.isEmpty() );
            //
            justTakeData = stack.pop();
            assertTrue( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()

    
    /** Funktions-Test: "top()" (und zuvor "push()"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_peek_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data testData = new Data( 47 );
            stack.push( testData );
            assertTrue( testData == stack.top() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "pop()" (und zuvor "push()"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_dequeue_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data testData = new Data( 53 );
            stack.push( testData );
            assertTrue( testData == stack.pop() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "push()" (und zuvor "pop()"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_sequenceMultiplePushPop_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int i=13; --i>=0; ){
                final Data testData = new Data( i );
                stack.push( testData );
                assertTrue( testData == stack.pop() );
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Funktions-Test: "clear()" (und zuvor "push()"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_clear_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardAnyData = new Data( 42 );
            guardStack.push( guardAnyData );
            assertFalse( "Guard test failed - stack shall NOT be empty",  guardStack.isEmpty() );
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash/error yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data anyData = new Data( 11 );
            stack.push( anyData );
            stack.clear();
            assertTrue( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "clear()" ("sofort"). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_clear_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardAnyData = new Data( 42 );
            guardStack.push( guardAnyData );
            assertFalse( "Guard test failed - stack shall NOT be empty",  guardStack.isEmpty() );
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash/error yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            stack.clear();
            assertTrue( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: "clear()" (in Sequenz). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_sequencePushClearIsempty_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardAnyData = new Data( 42 );
            guardStack.push( guardAnyData );
            assertFalse( "Guard test failed - stack shall NOT be empty",  guardStack.isEmpty() );
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash/error yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stackDepth=13; --stackDepth>=0; ){
                for( int val=0; val<stackDepth; val++ ){
                    stack.push( new Data( val ));
                }//for
                stack.clear();
                assertTrue( stack.isEmpty() );
            }//for
            stack.push( new Data( 42 ));
            stack.clear();
            assertTrue( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
         
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()   
     
    /** Funktions-Test: "clear()" (in Sequenz). */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_sequencePushClearIsempty_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stackDepth=13; --stackDepth>=0; ){
                for( int val=0; val<stackDepth; val++ ){
                    stack.push( new Data( val ));
                    assertFalse( stack.isEmpty() );
                }//for
                stack.clear();
                assertTrue( stack.isEmpty() );
            }//for
            stack.push( new Data( 42 ));
            assertFalse( stack.isEmpty() );
            stack.clear();
            assertTrue( stack.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    /** Kombinierter Funktions-Test: "Stack". */
    @Test( timeout = commonLimit )
    public void tol_3n_behavior_Stack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            
            int startIndexWithoutOffset = 0;
            for( int testRun=0; testRun<1_300; testRun+=100 ){
                
                assertTrue( stack.isEmpty() );
                startIndexWithoutOffset = 0;
                for( int i=startIndexWithoutOffset+testRun; i<42+startIndexWithoutOffset+testRun; i++ ){
                    final Data dataOrg = new Data( i );
                    stack.push( dataOrg );
                    assertFalse( stack.isEmpty() );
                    final Data dataTop = stack.top();
                    assertTrue( dataOrg == dataTop );
                }//for
                
                //startIndexWithoutOffset = 0;   // unchanged
                for( int i=42+startIndexWithoutOffset+testRun; --i>=startIndexWithoutOffset+testRun; ){
                    assertFalse( stack.isEmpty() );
                    final Data dataCpy = new Data( i );
                    final Data dataTop = stack.top();
                    final Data dataPop = stack.pop();
                    assertTrue( dataTop == dataPop );
                    assertTrue( dataPop.equals( dataCpy ));
                }//for
                assertTrue( stack.isEmpty() );
                
                assertTrue( stack.isEmpty() );
                
                startIndexWithoutOffset += 43;
                for( int i=startIndexWithoutOffset+testRun; i<13+startIndexWithoutOffset+testRun; i++ ){
                    final Data dataOrg = new Data( i );
                    stack.push( dataOrg );
                    assertFalse( stack.isEmpty() );
                    final Data dataTop = stack.top();
                    assertTrue( dataOrg == dataTop );
                }//for
                stack.clear();
                assertTrue( stack.isEmpty() );               
            }//for
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.C, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsTwoPoints ));
        }//if
    }//method()
    
    
    
    
    
    //##########################################################################
    //###
    //###   D / "4s"
    //###    
    
    /** Funktions-Test: parameter is null" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_parameter_null(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardStack.push( guardSomeData );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            boolean expectedExceptionOccured = false;
            try{
                stack.push( null );                                          // ATTENTION! : This will raise NO TestSupportException -> hence, do NOT check for it ;-)
                fail( "undetected illegal argument -> null accepted" );
            }catch( final AssertionError | IllegalArgumentException ex ){
                // "TS.examineInternallyRaisedTestSupportExceptionForIllegalArgumentCauseAndReact( ex );" wont work, since TestSupportException is NOT thrown
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: underflow after "pop()" - 1x"push()", 2x"pop()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterPop_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardStack.push( guardSomeData );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data someData = new Data( 13 );
            stack.push( someData );
            assertTrue( someData == stack.pop() );
            boolean expectedExceptionOccured = false;
            try{
                @SuppressWarnings("unused")
                final Data notExisting = stack.pop();
                fail( "undetected underflow" );
            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: underflow after "top()" - 1x"push()", 1x"pop()", !x"top()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterTop_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardStack.push( guardSomeData );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data someData = new Data( 13 );
            stack.push( someData );
            assertTrue( someData == stack.pop() );
            boolean expectedExceptionOccured = false;
            try{
                @SuppressWarnings("unused")
                final Data notExisting = stack.top();
                fail( "undetected underflow" );
            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
         
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: underflow after "pop()" - idea: Nx"push()", 1x"clear()", 1x"pop()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterPop_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardStack.push( guardSomeData );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=13; --stillToDo>=0; )  stack.push( new Data( (int)( 65521 *Math.random() )));
            stack.clear();
            boolean expectedExceptionOccured = false;
            try{
                @SuppressWarnings("unused")
                final Data notExisting = stack.pop();
                fail( "undetected underflow" );
            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: underflow after "pop()" - idea: Nx"push()", 1x"clear()", 1x"top()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterTop_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardStack.push( guardSomeData );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            for( int stillToDo=13; --stillToDo>=0; )  stack.push( new Data( (int)( 65521 *Math.random() )));
            stack.clear();
            boolean expectedExceptionOccured = false;
            try{
                @SuppressWarnings("unused")
                final Data notExisting = stack.top();
                fail( "undetected underflow" );
            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: underflow after "pop()" - empty & 1x"pop()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterPop_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardStack.push( guardSomeData );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            boolean expectedExceptionOccured = false;
            try{
                @SuppressWarnings("unused")
                final Data notExisting = stack.pop();
                fail( "undetected underflow" );
            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Funktions-Test: underflow after "top()" - empty & 1x"top()" */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_underflowAfterTop_no3(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        //
        // guard test
        try{
            final Stack_I guardStack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Data guardSomeData = new Data( 42 );
            guardStack.push( guardSomeData );
            //\=> NO crash yet => guard test successfully passed ;-)
        }catch( final Exception ex ){
            fail( String.format( "Guard test failed -->> %s <<--",  ex.getMessage() ));
        }//try
        //\=> NO crash yet => guard test successfully passed ;-)
        //
        // start of actual test
        try{
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            boolean expectedExceptionOccured = false;
            try{
                @SuppressWarnings("unused")
                final Data notExisting = stack.top();
                fail( "undetected underflow" );
            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                expectedExceptionOccured = true;
            }//try
            assertTrue( expectedExceptionOccured );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Funktions-Test: Ping-Pong #1. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_pingPong_no1(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack1 = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Stack_I stack2 = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final int numberOfSequentialTestData = 7;
            final int numberOfCoreTestLoopRuns = 3;
            for( int value=numberOfSequentialTestData; --value>=0; ){
                stack1.push( new Data( value ));
            }//for
            
            for( int stillToDo=numberOfCoreTestLoopRuns; --stillToDo>=0; ){
                while( ! stack1.isEmpty() ){
                    stack2.push( stack1.pop() );
                }//while
                while( ! stack2.isEmpty() ){
                    stack1.push( stack2.pop() );
                }//while
            }//for
            
            for( int value=0; value<numberOfSequentialTestData; value++ ){
                final Data topData = stack1.pop();
                assertEquals( topData, new Data( value ));
            }//for
            assertTrue( stack1.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsTwoPoints ));
        }//if
    }//method()
    
    /** Funktions-Test: Ping-Pong #2. */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_pingPong_no2(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Stack_I stack1 = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final Stack_I stack2 = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            final int numberOfSequentialTestData = 23;
            final int numberOfCoreTestLoopRuns = 7;
            for( int value=numberOfSequentialTestData; --value>=0; ){
                stack1.push( new Data( value ));
            }//for
            
            for( int stillToDo=numberOfCoreTestLoopRuns; --stillToDo>=0; ){
                while( ! stack1.isEmpty() ){
                    stack2.push( stack1.top() );
                    stack2.push( stack1.pop() );
                }//while
                while( ! stack2.isEmpty() ){
                    final Data currentData = stack2.pop();
                    assertEquals( currentData, stack2.pop() );
                    stack1.push( currentData );
                }//while
            }//for
            
            for( int value=0; value<numberOfSequentialTestData; value++ ){
                final Data currentData = stack1.pop();
                assertEquals( currentData, new Data( value ));
            }//for
            assertTrue( stack1.isEmpty() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsTwoPoints ));
        }//if
    }//method()
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    /** Kombinierter Stress-Test: Funktion "Stack". */
    @Test( timeout = commonLimit )
    public void tol_4s_behavior_stressStack(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "Stack";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            boolean expectedExceptionOccured = false;
            final Stack_I stack = (Stack_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            
            // keep things easy at beginning    ( similar testBehavior_Stack() )
            for( int testRun=0; testRun<19_000; testRun+=1_000 ){
                //
                assertTrue( stack.isEmpty() );
                for( int i=0+testRun; i<42+testRun; i++ ){
                    final Data dataOrg = new Data( i );
                    stack.push( dataOrg );
                    assertFalse( stack.isEmpty() );
                    stack.top();
                    stack.top();
                    stack.top();
                    final Data dataTop = stack.top();
                    assertTrue( dataOrg == dataTop );
                }//for
                
                for( int i=42+testRun; --i>=0+testRun; ){
                    assertFalse( stack.isEmpty() );
                    stack.top();
                    stack.top();
                    stack.top();
                    final Data dataCpy = new Data( i );
                    final Data dataTop = stack.top();
                    final Data dataPop = stack.pop();
                    assertTrue( dataTop == dataPop );
                    assertTrue( dataPop.equals( dataCpy ));
                }//for
                assertTrue( stack.isEmpty() );
                //
            }//for
            
            // now stress it
            for( int testRun=0; testRun<17_000_000; testRun+=1_000_000 ){
                //
                assertTrue( stack.isEmpty() );
                for( int i=0+testRun; i<42+testRun; i++ ){
                    final Data data = new Data( i );
                    stack.push( data );
                    assertFalse( stack.isEmpty() );
                }//for
                
                for( int i=42+testRun; --i>=0+testRun; ){
                    assertFalse( stack.isEmpty() );
                    final Data dataCpy = new Data( i );
                    final Data dataOrg = stack.pop();
                    assertTrue( dataOrg.equals( dataCpy ));
                }//for
                assertTrue( stack.isEmpty() );
                
                try{
                    // causing underflow(s) by top()
                    for( int stillToDo=3; --stillToDo>=0; ){
                        expectedExceptionOccured = false;
                        try{
                            @SuppressWarnings("unused")
                            final Data notExisting = stack.top();
                            fail( "undetected underflow" );
                        }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                            expectedExceptionOccured = true;
                        }//try
                        assertTrue( expectedExceptionOccured );
                    }//for
                    //
                    // causing underflow(s) by pop()
                    for( int stillToDo=3; --stillToDo>=0; ){
                        expectedExceptionOccured = false;
                        try{
                            @SuppressWarnings("unused")
                            final Data notExisting = stack.pop();
                            fail( "undetected underflow" );
                        }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                            expectedExceptionOccured = true;
                        }//try
                        assertTrue( expectedExceptionOccured );
                    }//for
                }catch( final NullPointerException ex ){
                    fail( ex.getMessage() );
                }catch( final Exception ex ){
                    TS.printInternalNote4Exception( "stack underflow", ex, testName );
                }//try
                //
                assertTrue( stack.isEmpty() );
                
                try{
                    for( int stillToDo=3; --stillToDo>=0; ){
                        //
                        // fill stack & 1* clear() before underflow
                        for( int i=0; i<7; i++ ){
                            final Data data = new Data( i );
                            stack.push( data );
                            assertFalse( stack.isEmpty() );
                        }//for
                        stack.clear();
                        //
                        // causing underflow(s) by pop()
                        assertTrue( stack.isEmpty() );
                        expectedExceptionOccured = false;
                        try{
                            @SuppressWarnings("unused")
                            final Data notExisting = stack.pop();
                            fail( "undetected underflow" );
                        }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                            expectedExceptionOccured = true;
                        }//try
                        assertTrue( expectedExceptionOccured );
                        //
                        // causing underflow(s) by top()
                        expectedExceptionOccured = false;
                        try{
                            @SuppressWarnings("unused")
                            final Data notExisting = stack.top();
                            fail( "undetected underflow" );
                        }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                            expectedExceptionOccured = true;
                        }//try
                        assertTrue( expectedExceptionOccured );
                        //
                        assertTrue( stack.isEmpty() );
                        
                        // fill stack & 1* clear() before several underflows
                        for( int i=0; i<5; i++ ){
                            final Data data = new Data( i );
                            stack.push( data );
                            assertFalse( stack.isEmpty() );
                        }//for
                        stack.clear();
                        //
                        // causing underflow(s) by top()
                        for( int i=0; i<3; i++ ){
                            expectedExceptionOccured = false;
                            try{
                                @SuppressWarnings("unused")
                                final Data notExisting = stack.top();
                                fail( "undetected underflow" );
                            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                                expectedExceptionOccured = true;
                            }//try
                            assertTrue( expectedExceptionOccured );
                        }//for
                        //
                        // causing underflow(s) by pop()
                        for( int i=0; i<3; i++ ){
                            expectedExceptionOccured = false;
                            try{
                                @SuppressWarnings("unused")
                                final Data notExisting = stack.pop();
                                fail( "undetected underflow" );
                            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                                expectedExceptionOccured = true;
                            }//try
                            assertTrue( expectedExceptionOccured );
                        }//for
                        //
                        assertTrue( stack.isEmpty() );
                        
                        // n * clear() before several unflow
                        for( int i=0; i<stillToDo+1; i++ ){
                            stack.clear();
                        }//for
                        //
                        // causing underflow(s) by pop()
                        for( int i=0; i<stillToDo+3; i++ ){
                            expectedExceptionOccured = false;
                            try{
                                @SuppressWarnings("unused")
                                final Data notExisting = stack.pop();
                                fail( "undetected underflow" );
                            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                                expectedExceptionOccured = true;
                            }//try
                            assertTrue( expectedExceptionOccured );
                        }//for
                        //
                        // causing underflow(s) by top()
                        for( int i=0; i<stillToDo+3; i++ ){
                            expectedExceptionOccured = false;
                            try{
                                @SuppressWarnings("unused")
                                final Data notExisting = stack.top();
                                fail( "undetected underflow" );
                            }catch( final AssertionError | EmptyStackException | IllegalStateException ex ){
                                expectedExceptionOccured = true;
                            }//try
                            assertTrue( expectedExceptionOccured );
                        }//for
                        //
                        assertTrue( stack.isEmpty() );
                    }//for
                }catch( final NullPointerException ex ){
                    fail( ex.getMessage() );
                }catch( final Exception ex ){
                    TS.printInternalNote4Exception( "stack underflow", ex, testName );
                }//try
                assertTrue( stack.isEmpty() );
            }//for
            
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.D, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsThreePoints ));
        }//if
    }//method()    
    
    
    
    
    
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            TS.runTestSetupBeforeAnyUnitTestStarts( dbManager, exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed - the "normal @AfterClass" as it should be
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()                                                 // BlueJ keeps state, otherwise
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 42;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A2;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evaluation or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
  //final static private boolean enableDbgOutputAfterEachTest  =  ( 0 != ( dbgConfigurationVector & 0x0080_0000 ));
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
