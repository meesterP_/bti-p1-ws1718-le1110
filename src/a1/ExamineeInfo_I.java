package a1;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Ueber das Interface ExamineeInfo_I wird der Zugriff auf Information(en) ueber den Pruefling sicher gestellt.<br />
 * <br />
 * Das Interface ExamineeInfo_I definiert die Funktionalitaet moeglicher Implementierungen und fordert die entsprechenden Methoden ein.
 * Objekte, die diesem Interface genuegen werden verwendet um Sie zu identifizieren.<br />
 * <br />
 * Die von Ihnen zu implementierenden Klasse ExamineeInfo muss
 * <ul>
 *     <li>einen oeffentlichen Kontruktor aufweisen, der der folgenden Signatur genuegt:<br />
 *         <code>ExamineeInfo()</code>
 *     </li>
 * </ul>
 * Eine genaue Auflistung der Anforderungen an die zu implementierende Klasse findet sich auf dem Aufgabenzettel,
 * sowie weitere (Teil-)Aufgaben, die geloest werden muessen.
 *<br />
 *<br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public interface ExamineeInfo_I {
    
    /**
     * getExamineeSurName() liefert den Nachnamen des Prueflings in Kleinbuchstaben ohne moegliche Bindestriche oder Leerzeichen.
     * (Mit Kleinbuchstaben sind die 26 Kleinbuchstaben a bis z des (modernen) lateinischen Alphabets gemeint.)
     * Sollten Sie mehr als einen Nachnamen haben, ist derjenige zu verwenden mit dem Sie auch bei der HAW (zuerst) gefuehrt werden.
     * Im Zweifelsfall schauen Sie einfach auf Ihrem Studentenausweis nach, der vor Ihnen auf den Tisch liegt.
     * Ferner muessen moegliche Namenszusaetze (wie z.B. von, van, ten, oder Mc) hinten angestellt werden.<br />
     * Beispielsweise wuerde &quot;von Cro&yuml;-Grae&szlig;ler&quot; zu &quot;croygraesslervon&quot;.<br />
     * 
     * @return Nachname des Prueflings in Kleinbuchstaben.
     */
    public String getExamineeSurName();
    
    /**
     * getExamineeFirstName() liefert den Vornamen des Prueflings in Kleinbuchstaben ohne moegliche Bindestriche oder Leerzeichen.
     * (Mit Kleinbuchstaben sind die 26 Kleinbuchstaben a bis z des (modernen) lateinischen Alphabets gemeint.)
     * Sollten Sie mehr als einen Vornamen haben, ist derjenige zu verwenden mit dem Sie auch bei der HAW (zuerst) gefuehrt werden.
     * Im Zweifelsfall schauen Sie einfach auf Ihrem Studentenausweis nach, der vor Ihnen auf den Tisch liegt.<br />
     * Beispielsweise wuerde &quot;Andr&eacute;-&Euml;v&iuml;no&quot; zu &quot;andreevino&quot;.<br />
     * 
     * @return Vorname des Prueflings in Kleinbuchstaben.
     */
    public String getExamineeFirstName();

}//interface
