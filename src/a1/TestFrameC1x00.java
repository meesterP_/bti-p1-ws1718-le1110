package a1;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
//
import static supportC1x00.Configuration.dbgConfigurationVector;
import static supportC1x00.Herald.Medium.*;
import static supportC1x00.PointDefinition.*;
//
//
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
//
import supportC1x00.Herald;
import supportC1x00.TC;
import supportC1x00.TE;
import supportC1x00.TL;
import supportC1x00.TS;
import supportC1x00.TestResult;
import supportC1x00.TestResultDataBaseManager;
import supportC1x00.TestSupportException;
import supportC1x00.TestTopic;
import supportC1x00.YattbCounter;


/**
 * LabExam1110_4XIB1-P1    (PTP-BlueJ)      [ex LabExam2G Demo and Reference]<br />
 *<br />
 * Diese Sammlung von Tests soll nur die Sicherheit vermitteln, dass Sie die Aufgabe richtig verstanden haben
 * und Ihnen nur eine Idee geben, wie gut Sie die Aufgabe geloest haben.<br />
 * Es ist insbesondere NICHT geplant, dass Sie alle Tests verstehen. Vermutlich gibt es so viele Tests,
 * dass Sie gar nicht die Zeit haben sich in alle Tests einzuarbeiten.<br />
 * Dass von den Tests dieser Testsammlung keine Fehler gefunden wurden, kann NICHT als Beweis dienen,
 * dass der Code fehlerfrei ist. Es liegt in Ihrer Verantwortung sicher zu stellen,
 * dass Sie fehlerfreien Code geschrieben haben.<br />
 * Bei der Bewertung werden u.U. andere - konkret : modifizierte und haertere Tests - verwendet.
 * <br />
 * <br />
 * VCS: git@BitBucket.org:schaefers/LabExam2G.git
 *
 * @author   Michael Schaefers ; P1@Hamburg-UAS.eu 
 * @version  LabExam1110_4XIB1-P1_162v12_170228_v01
 */
public class TestFrameC1x00 {
    
    /** Ausgabe auf Bildschirm zur visuellen Kontrolle (fuer Studenten idR. abgeschaltet, wird aber IMMER mit angestartet). */
    @Test( timeout = commonLimit )
    public void tol_0i_printExamineeInfoForManualReview(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        final boolean dbgLocalOutputEnable = ( 0 != ( dbgConfigurationVector & 0x0100 ));
        if( dbgLocalOutputEnable ){
            System.out.printf( "\n\n" );
            System.out.printf( "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" );
            System.out.printf( "%s():\n",  testName );
            System.out.printf( "\n\n" );
            
            
            final String requestedRefTypeName = "ExamineeInfo";
            final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
            try{
                final ExamineeInfo_I examineeInfo = (ExamineeInfo_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
                
                final int snl = examineeInfo.getExamineeSurName().length();
                final int fnl = examineeInfo.getExamineeFirstName().length();
                final int reservedSpace = ((snl<fnl) ? fnl : snl);
                
                // print examinee information
                System.out.printf( "%67s", "" );    for( int i=reservedSpace; --i>=0; ){ System.out.printf( "v" ); }    System.out.printf( "\n" );
                System.out.printf( "Nachname / Familienname bzw. surname / family name / last name :   %-"+(reservedSpace+3)+"s<<<===\n",  examineeInfo.getExamineeSurName() );
                System.out.printf( "Vorname / Rufname       bzw. first name                        :   %-"+(reservedSpace+3)+"s<<<===\n",  examineeInfo.getExamineeFirstName() );
                System.out.printf( "%67s", "" );    for( int i=reservedSpace; --i>=0; ){ System.out.printf( "^" ); }    System.out.printf( "\n" );
                System.out.printf( "\n\n" );
                
                TS.printDetailedInfoAboutClass( requestedRefTypeWithPath );
                System.out.printf( "\n" );
                TS.printDetailedInfoAboutObject( examineeInfo, "examineeInfo" );
                //
                if( TS.isActualMethod( examineeInfo.getClass(), "toString", String.class, null )){
                    System.out.printf( "~.toString(): \"%s\"     again ;-)\n",  examineeInfo.toString() );
                }else{
                    System.out.printf( "NO! toString() implemented by class \"%s\" itself\n",  examineeInfo.getClass().getSimpleName() );
                }//if
                System.out.printf( "\n\n" );
                
                System.out.printf( "%s_%s\n",  examineeInfo.getExamineeSurName(), examineeInfo.getExamineeFirstName() );
            }catch( final TestSupportException ex ){
                ex.printStackTrace();
                fail( ex.getMessage() );
            }//try
            
            System.out.printf( "\n" );
            System.out.printf( "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" );
            System.out.printf( "\n\n" );
        }//if
        
        // at least the unit test was NOT destroyed by student ;-)
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.DBGPRINT ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()  
    
    
    
    /** Existenz-Test: "ExamineeInfo". */
    @Test( timeout = commonLimit )
    public void tol_0i_classExistence_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.EXISTENCE ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test einiger Eigenschaften des Referenz-Typs "ExamineeInfo". */
    @Test( timeout = commonLimit )
    public void tol_0i_properties_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            //
            assertTrue(                                     TS.isClass( classUnderTest ));                                                                          // objects are created for tests ;-)
            assertTrue( "false class access modifier",      TS.isAccessModifierSet( classUnderTest, Modifier.PUBLIC ));
            assertTrue( "requested supertype missing",      TS.isImplementing( classUnderTest, "ExamineeInfo_I" ));
            assertTrue( "requested constructor missing",    TS.isConstructor( classUnderTest,                                  null ));
            assertTrue( "false constructor access modifier",TS.isAccessModifierSet( classUnderTest,                                       null, Modifier.PUBLIC ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ExamineeInfo" - Access Modifier Methoden. */
    @Test( timeout = commonLimit )
    public void tol_0i_propertiesMethods_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "getExamineeSurName",   String.class, null ));
            assertTrue( "requested method missing",     TS.isMethod(            classUnderTest, "getExamineeFirstName", String.class, null ));
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "getExamineeSurName",   String.class, null, Modifier.PUBLIC )); // -D interface ;-)
            assertTrue( "false method access modifier", TS.isAccessModifierSet( classUnderTest, "getExamineeFirstName", String.class, null, Modifier.PUBLIC )); // -D interface ;-)
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ExamineeInfo" - Access Modifier Variablen. */
    @Test( timeout = commonLimit )
    public void tol_0i_propertiesFields_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            assertTrue( "false field access modifier",  TS.allVariableFieldAccessModifiersPrivate( classUnderTest ));
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ExamineeInfo" - Schreibweise Methoden. */
    @Test( timeout = commonLimit )
    public void tol_0i_notationMethods_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidMethodNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "method name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Test Eigenschaften "ExamineeInfo" - Schreibweise Variablen. */
    @Test( timeout = commonLimit )
    public void tol_0i_notationFields_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final Class<?> classUnderTest = Class.forName( requestedRefTypeWithPath );
            final String faultExample = TS.hasInvalidFieldNotation( classUnderTest );
            if( null != faultExample ){
                fail( String.format( "field name: \"%s\" does NOT follow the VERY JAVA LAW !",  faultExample ));
            }//if
        }catch( final ClassNotFoundException ex ){
            fail( String.format( "can NOT find \"%s\" -> %s",  requestedRefTypeName, ex.getMessage() ));
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.PROPERTIES ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    /** Grundsaetzlicher Test: ExamineeInfo erzeugen. */
    @Test( timeout = commonLimit )
    public void tol_0i_objectCreation_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            @SuppressWarnings("unused")
            final ExamineeInfo_I examineeInfo = (ExamineeInfo_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.CREATION ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
     
    
    
    /** Einfacher Test: Funktion "ExamineeInfo". */
    @Test( timeout = commonLimit )
    public void tol_0i_behavior_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final ExamineeInfo_I examineeInfo = (ExamineeInfo_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            
            final Pattern pattern = Pattern.compile( "^[a-z]+$" );
            final Matcher matcherSurName = pattern.matcher( examineeInfo.getExamineeSurName() );
            assertTrue( matcherSurName.find() );
            final Matcher matcherFirstName = pattern.matcher( examineeInfo.getExamineeFirstName() );
            assertTrue( matcherFirstName.find() );
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    /** Konsistenz-Test: Entspricht Inhalt von me.txt der Implementierung von ExamineeInfo. */
    @Test( timeout = commonLimit )
    public void tol_0i_consistency_ExamineeInfo(){
        final String testName = new Object(){}.getClass().getEnclosingMethod().getName();
        
        final String requestedRefTypeName = "ExamineeInfo";
        final String requestedRefTypeWithPath = exerciseAsResultOfFileLocation+"."+requestedRefTypeName;
        try{
            final ExamineeInfo_I examineeInfo = (ExamineeInfo_I)( TS.generateRequestedObject( requestedRefTypeWithPath ));
            
            //##################################################################
            //###
            //###  __???__<160226> BlueJ <-> Eclipse
            //###  me.txt -> "a1/me.txt" <-> "src/a1/me.txt"
            //###
            //### "Class".getResource( ... ) & "Class".getResourceAsStream( ... ) search in bin :-(
            //### e.g. :  final BufferedReader br = new BufferedReader( new InputStreamReader( ExamineeInfo_I.class.getResourceAsStream( "me.txt" )));
            //
            
            final String examineeInfoBlueJFilePath   = System.getProperty("user.dir")+File.separator+"a1"+File.separator+"me.txt";
            final String examineeInfoEclipseFilePath = System.getProperty("user.dir")+File.separator+"src"+File.separator+"a1"+File.separator+"me.txt";
            String examineeInfoFilePath = null;
            if( new File( examineeInfoBlueJFilePath ).exists() ){
                // BlueJ expected
                examineeInfoFilePath = examineeInfoBlueJFilePath; 
            }else if( new File( examineeInfoEclipseFilePath ).exists() ){
                // Eclipse expected
                examineeInfoFilePath = examineeInfoEclipseFilePath;
            }//if
            if( null != examineeInfoFilePath ){
                FileReader fr = null;                                           // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                BufferedReader br = null;                                       // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                try/*(                                                          // BlueJ BUG - AutoClosable is NOT supported
                    final FileReader fr = new FileReader( examineeInfoFilePath );              // BlueJ BUG - AutoClosable is NOT supported
                    final BufferedReader br = new BufferedReader( fr );                        // BlueJ BUG - AutoClosable is NOT supported
                )*/{                                                                           // BlueJ BUG - AutoClosable is NOT supported
                    fr = new FileReader( examineeInfoFilePath );                // WorkAround for BlueJ BUG - AutoClosable is NOT supported
                    br = new BufferedReader( fr );                              // WorkAround for BlueJ BUG - AutoClosable is NOT supported
                    final String examineeDataFile = br.readLine();
                    assertEquals( examineeDataFile,  examineeInfo.getExamineeSurName() + "_" + examineeInfo.getExamineeFirstName() );
                    assertNull( br.readLine() );
                }catch( final FileNotFoundException ex ){
                    fail( ex.getMessage() );
                }catch( final IOException ex ){
                    fail( ex.getMessage() );
                }finally{                                                       // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                    try{                                                        // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                        br.close();                                             // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                        fr.close();                                             // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                    }catch( final IOException ex ){                             // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                        ex.printStackTrace();                                   // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                        throw new IllegalStateException( String.format( "this should NOT have happened -> \"%s\"\n",  ex.getMessage() ));
                    }//try                                                      // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
                }//try                                                          // result of WorkAround for BlueJ BUG - AutoClosable is NOT supported
            }else{
                fail( "can NOT loacte me.txt" );
            }//if
        }catch( final TestSupportException ex ){
            fail( ex.getMessage() );
        }//try
        
        if( enableAutomaticEvaluation ){
            dbManager.enterLocally( new TestTopic( TL.I, exercise, TC.BEHAVIOR ),  new TestResult( testName, countsOnePoint ));
        }//if
    }//method()
    
    
    
    
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @BeforeClass
     * ============
     */
    /** WORKAROUND, since "@BeforeClasss" is NOT WORKING with BlueJ <= 3.1.7 */
    @BeforeClass
    public static void runSetupBeforeAnyUnitTestStarts(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG       
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheStart = !yattbCounter.isFirstStarting();
            if( notTheStart ){                                                  // YATTB: Under BlueJ @BeforeClass seems to be executed before each test!
                //=> this is NOT the first test => it has to wait until "@BeforeClass" has finished
                yattbCounter.waitUntilFirstHasFinished();
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        
        // THIS METHOD HAS AN INDIVIDUAL IMPLEMENTATION BODY ON PURPOSE
        //
        System.out.printf( "Informationen zum Environment:\n" );
        System.out.printf( "    Java:   %s bzw. %s\n",  System.getProperty( "java.specification.version" ), System.getProperty( "java.version" ) );
        System.out.printf( "    O.-P.:  %s\n",          new Object().getClass().getPackage() );
        System.out.printf( "    WPI computer :                                              %s\n",    supportC1x00.CentralVersionData.centralWPIComputerVersionID );
        System.out.printf( "    Engine   (test support routine collection) :                %s\n",    supportC1x00.CentralVersionData.centralTestSupportVersionID );
        System.out.printf( "    Test     (test defined by version ID) :                     %s\n",    supportC1x00.CentralVersionData.centralLabExamVersionID );
        System.out.printf( "    Exam     (test defined by path) :                           %s%s\n",  TestFrameC1x00.class.getProtectionDomain().getCodeSource().getLocation().getPath(), exerciseAsResultOfFileLocation );  // __???__<170104> ??? BlueJ ???
        System.out.printf( "    Exercise (test defined by internal test definition/enum) :  %s\n",    exercise );
        System.out.printf( "\n" );
        System.out.printf( "################################################################################\n" );
        System.out.printf( "\n\n" );
        
        guaranteeExerciseConsistency( exercise.toString().toUpperCase(), exerciseAsResultOfFileLocation.toUpperCase() );
        if( enableAutomaticEvaluation ){
            dbManager = new TestResultDataBaseManager( exercise );
        }//if
        yattbCounter.signalThatFirstHasFinished();
    }//method()
    //
    private static void guaranteeExerciseConsistency(
        final String  stringAsResultOfEnum,
        final String  stringAsResultOfPath
    ){
        if( ! stringAsResultOfEnum.equals( stringAsResultOfPath )){
            Herald.proclaimMessage( SYS_ERR,  String.format(
                "Uuupps : Unexpected internal situation\n    This might indicate an internal coding error\n    Call schaefers\n\nSETUP ERROR :  %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
            throw new IllegalStateException( String.format(
                "Uuupps : unexpected internal situation - this might indicate an internal coding error => call schaefers -> SETUP ERROR %s != %s",
                stringAsResultOfEnum,
                stringAsResultOfPath
            ));
        }//if
    }//method()        
    
    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     * @AfterClass
     * ===========
     */
    /** WORKAROUND, since "@AfterClass" is NOT WORKING with BlueJ <= 3.1.7 */
    @AfterClass
    public static void runTearDownAfterAllUnitTestsHaveFinished(){
        //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
        // the following code  is related to the BlueJ JUnit BUG
        final boolean bluejBugFixSupportEnable = ( 0 != ( dbgConfigurationVector & 0x0002_0000 ));
        if( bluejBugFixSupportEnable ){
            Herald.proclaimTestCount( SYS_OUT,  yattbCounter.getStartedCounter() );
        }//if
        //
        final boolean bluejBugFixWorkAroundEnable = ( 0 != ( dbgConfigurationVector & 0x0001_0000 ));
        if( bluejBugFixWorkAroundEnable ){
            final boolean notTheEnd = !yattbCounter.isLastFinishing();
            if( notTheEnd ){
                return;
            }//if
        }//if
        //the code above/before  is related to the BlueJ JUnit BUG
        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //=>this point of code is only reached in case the last test was executed
        //  the "normal @AfterClass" as it should be
        //  NOTE: At least @AfterClass seems to be executed after @Test
        //        hence all @Test have finished
        
        if( enableAutomaticEvaluation ){
            TS.runTestTearDownAfterAllUnitTestsHaveFinished( dbManager );       // <<<<==== the actual method body
            dbManager.reset();                                                  // BlueJ keeps state, otherwise
        }//if
        yattbCounter.reset();                                                   // BlueJ keeps state, otherwise
    }//method()
    
    
    
    
    
    
    
    
    
    
    // constant(s)
    
    // limit for test time
    final static private int commonLimit = 4_000;                               // timeout resp. max. number of ms for test
    
    // BlueJ BUG workaround(s)
    final static private int numberOfTests = 10;
    final static private YattbCounter yattbCounter = new YattbCounter( numberOfTests );
    
    // exercise related
    final static private TE exercise = TE.A1;
    final static private String exerciseAsResultOfFileLocation = new Object(){}.getClass().getPackage().getName();
    
    // automatic evaluation or more detailed access to debugManager (as result of BlueJ BUG workaround)
    final static private boolean enableAutomaticEvaluation  =  ( 0 > dbgConfigurationVector );
    
  //final static private boolean enableDbgOutputAfterEachTest  =  ( 0 != ( dbgConfigurationVector & 0x0080_0000 ));
    
    
    
    // variable(s) - since the methods are static, the following variables have to be static
    
    // TestFrame "state"
    static private TestResultDataBaseManager  dbManager  =  ( enableAutomaticEvaluation )  ?  new TestResultDataBaseManager( exercise )  :  null;
    
}//class
